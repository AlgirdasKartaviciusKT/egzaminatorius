using Egzaminatorius.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Controlers
{
    class commentsControler

    {
		public void openCommentForm(int id  )
		{
            Views.comment_form commentsForm = new Views.comment_form(id);
            commentsForm.Show();
        }
		
		public bool checkData(string text)
		{
            if(text.Length > 0)
            {
                return true;
            }
            return false;
			
		}
		
		public void delete(int index)
		{
            
            Models.Comment delete = new Models.Comment();
            delete.DeleteComment(index);
        }
		
		public void rate(bool rating, int index)
		{
            if (rating)
            {
                Models.Comment comment = new Models.Comment();
                comment.IncreamentLike(index);
            }
            else
            {
                Models.Comment comment = new Models.Comment();
                comment.IncreamentDislike(index);
            }
			
		}
		
		public string review(int index)
		{
            Models.Comment comment = new Models.Comment();
            string text = comment.Select(index);
            return text;

        }
	
		public void getComment(int id )
		{
            Views.comment_page commentList = new Views.comment_page(id);
            commentList.Show();
        }
		
		public void back(int id)
		{
            Article article = new Article();
            Article read = article.SelectArticleById(id);
            Views.article_page aa = new Views.article_page(read, new articleController(), false);
            aa.Show();
            /* Views.home_page a = new Views.home_page();
             a.Show();*/

        }
		
	}
	
}
