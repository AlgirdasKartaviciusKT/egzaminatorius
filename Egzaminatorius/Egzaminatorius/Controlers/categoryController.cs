using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Egzaminatorius.Models;

namespace Egzaminatorius.Controlers
{
	public class categoryController
	{
		
		public static void openCategoryList(  )
		{
            List<topic> topics = topic.SelectTopics();
            Views.category_list_page categoryList = new Views.category_list_page(topics, new categoryController());
            categoryList.Show();
        }
		
		public bool verify( string name )
		{
            if (!topic.topicExists(name))
            {
                if (name.Length < 3 || name.Length > 40) return false;
                else return true;
            }
            else
                return false;
		}
		
		public bool createArticle( string name )
		{
            if (verify(name))
            {
                return topic.insert(name);
            }
            else return false;
		}
		
	}
	
}
