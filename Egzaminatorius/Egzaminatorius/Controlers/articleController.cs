using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Egzaminatorius.Models;

namespace Egzaminatorius.Controlers { 

    public class articleController
	{
        public void getArticleList(string category)
        {
            List<Article> articles = Article.Select(category);
            List<Article> drafts = Article.SelectDrafts(category);
            Views.article_list_page articleList = new Views.article_list_page(articles, drafts, this, category);
            show(articleList);
        }
        private void show(Views.article_list_page article_List)
        {
            article_List.Show();
        }
        public static void getArticle( string headline )
        {
            Article article = new Article();
            Article read = article.SelectArticle(headline);
            show(read);
           
		}
        public static void show(Article read)
        {
            Views.article_page aa = new Views.article_page(read, new articleController(), false);
            aa.Show();
        }
		
		public bool createArticle( string headline, string content, string category, bool publish )
		{
            if(verify(headline))
            {
                if (publish)
                {
                    return Article.SavePublication(headline, content, category, sessionInfo.UserID);
                }
                else
                {
                    return Article.SaveDraft(headline, content, category, sessionInfo.UserID);
                }
            }
            return false;
		}

        public bool updateArticle(Article art, bool publish)
        {
            if (verify(art.headline))
            {
                if (publish)
                {
                    art.articleState = "Pending";
                    Article.update(art);
                }
                else
                {
                    art.articleState = "Draft";
                    Article.update(art);
                }
                return true;
            }
            return false;
        }
		
		public bool verify( string headline )
		{
            return (!Article.articleExists(headline));
		}
		
		public void getArticleContent(  )
		{
			
		}
		
		public void approveArticle( Article ar )
		{
            ar.articleState = "Approved";
            getWaitingList(Article.update(ar));
		}
		
		public void rejectArticle( Article ar)
		{
            ar.articleState = "Rejected";
            getWaitingList(Article.update(ar));
        }
		
		public void removeArticle( string headline )
		{
            Article.remove(headline);
            getWaitingList();
		}
		
        public void judgeArticle(string headline)
        {
            Article aa = new Article();
            aa = aa.SelectArticle(headline);
            Views.article_page a = new Views.article_page(aa, this, true);
            a.Show();
        }

		public static void getWaitingList(  )
		{
            Views.article_approval_page ap = new Views.article_approval_page(Article.SelectPending(), Article.SelectAllApproved(), new articleController());
            ap.Show();
		}

        public static void getWaitingList(List<Article> arr)
        {
            Views.article_approval_page ap = new Views.article_approval_page(arr, Article.SelectAllApproved(), new articleController());
            ap.Show();
        }

        public void openArticleCreation( string category )
		{
            Views.article_creation_page ap = new Views.article_creation_page(category, this);
            ap.Show();
		}
		
		public void grade(bool rating, int index  )
		{
            if (rating)
            {
                Article article = new Article();
                article.IncreamentLike(index);
            }
            else
            {
                Article article = new Article();
                article.IncreamentDislike(index);
            }

        }
		
		public static void openArticleEdit( Article article )
		{
            Views.article_creation_page ap = new Views.article_creation_page(article.fkTopic, article, new articleController());
            ap.Show();
        }
		
	}
	
}
