﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Controlers
{
    public class achievementController
    {
        public List<Models.user_achievement> find_achievements()
        {
            Models.user_achievement achievDatabase = new Models.user_achievement();
            return achievDatabase.get_user_achievements(sessionInfo.UserID);
        }

        public void mark_achievement(int a_id, int u_id)
        {
            Models.user_achievement achievDatabase = new Models.user_achievement();
            achievDatabase.unmark(u_id);
            achievDatabase.mark(a_id, u_id);
        }

        public void check_for_new_achievements()
        {

        }

        public void check_unlock_req()
        {

        }

    }
}
