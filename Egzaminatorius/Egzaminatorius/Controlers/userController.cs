﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Controlers
{

    public class userController
    {
        Models.User userDatabase = new Models.User();

        public void logout()
        {
            if (userDatabase.check_status(sessionInfo.UserID))
                userDatabase.update_status(sessionInfo.UserID);
            sessionInfo.UserID = -1;
            sessionInfo.userRole = "";
        }

        public void block(int id)
        {
            userDatabase.block_user(id);
        }

        public List<Models.User> search_for_users(string line)
        {
            return userDatabase.search_for_matching_users(line);
        }

        public bool check_session_id()
        {
            if (sessionInfo.UserID != -1)
            {
                return true;
            }
            return false;
        }

        public int validate_log(string username, string password)
        {
            int retIndex = -1;
            int userId = userDatabase.check_existing(username, password);
            if (userId != -1)
            {
                userDatabase.update_status(userId);
                sessionInfo.UserID = userId;
                sessionInfo.userRole = userDatabase.find_user_data(userId).role;
                retIndex = 0; // successfuly logged in
            }
            else
                retIndex = 1; // Username/password don't match
            return retIndex;
        }

        public string validate_reg(string username, string password, string confpassword, string name, string surname, string email, DateTime birthday)
        {
            if(userDatabase.check_existing_username(username) != -1)
                return "Username is already taken";
            if (password != confpassword)
                return "Passwords do not match";
            if (!email.Contains('@'))
                return "Email is not valid";
            if (birthday == DateTime.Today)
                return "Invalid birth day";
            userDatabase.create_new(username, password, confpassword, name, surname, email, birthday);
            return "Successfully registered!";
        }

        public Models.User edit_user()
        {
           return userDatabase.find_user_data(sessionInfo.UserID);
        }

        public void confirm_edit(string name, string surname, string email, DateTime birthday)
        {
            userDatabase.update_info(sessionInfo.UserID, name, surname, email, birthday);
        }

    }
}
