﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Controlers
{
    class test_controller
    {
        public void openTestList()
        {
            Views.test_list_page test_List_Page = new Views.test_list_page();
            test_List_Page.Show();
        }

        public void openTestInfo()
        {
            Views.test_page test_Page = new Views.test_page();
            test_Page.Show();
        }

        public List<Models.User> openTestUserList(int id)
        {
            Models.UserTest userTest_Page = new Models.UserTest();
            List<Models.User> users = userTest_Page.getTestUsers(id);
            return users;
        }

        public List<Models.UserTest> openTestResult(int testId, int userId)
        {
            Models.UserTest userTest = new Models.UserTest();
            List<Models.UserTest> testResults = userTest.getTestResult(testId, userId);
            return testResults;
        }

        public void openTestForm()
        {
            Views.test_form_page test_Form_Page = new Views.test_form_page();
            test_Form_Page.Show();
        }

        // įrašyti testo informaciją
        public void createTest(Models.Test data)
        {
            Models.Test test = new Models.Test();
            test.insertTestData(data);
        }

        // įrašyti testo klausimus
        public void createTest(Models.TestQuestion data)
        {
            Models.TestQuestion testQ = new Models.TestQuestion();
            testQ.insertTestQuestions(data);
        }

        // įrašyti testo atsakymus
        public void createTest(Models.PossibleTestAnswer data)
        {
            Models.PossibleTestAnswer testA = new Models.PossibleTestAnswer();
            testA.insertTestAnswers(data);
        }

        public void verifyTestData()
        {

        }

        public void openEditTest()
        {

        }

        public void updateTest()
        {

        }

        public void openTest()
        {
            Views.test_solving_page test_Solving = new Views.test_solving_page();
            test_Solving.Show();
        }

        public void finishTest()
        {

        }

        public void verifySolving()
        {

        }

        public void deleteTest(int id)
        {
            Models.Test test = new Models.Test();
            test.deleteTest(id);
        }
    }
}
