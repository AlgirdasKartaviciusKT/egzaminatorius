﻿using Egzaminatorius.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius
{
    
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //DB server: (localdb)\MSSQLLocalDB
            //DB name: Egzaminatorius.CommentsContext

            Models.DbIniatilizer.Seed();

            
          
            
            Application.SetCompatibleTextRenderingDefault(false);
            sessionInfo.UserID = -1;
            sessionInfo.userRole = "";
            Application.Run(new login_page());
        }
    }
}
