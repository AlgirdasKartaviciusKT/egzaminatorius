﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class test_solving_page : Form
    {
        private int sec = 400; // laikinai (tikrinimui)
        public test_solving_page() 
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void test_solving_page_Load(object sender, EventArgs e)
        {
            int testId = Views.test_list_page.testId;
            label1.Text = "TestoID : " + testId + "";

            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 1;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Enabled = true;
        }

        private void finish_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.Hide();
            Views.test_results_page results = new Views.test_results_page();
            results.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            sec--;
            label2.Text = "Sprendimo laikas: " + sec / 60 + ":" + ((sec % 60) >= 10 ? (sec % 60).ToString() : "0" + (sec % 60));

            if (sec <= 0)
            {
                timer1.Stop();
                this.Hide();
                Views.test_results_page results = new Views.test_results_page();
                results.Show();
            }
        }
    }
}
