﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class achievement_page : Form
    {
        Controlers.achievementController achievControl;
        public achievement_page()
        {
            achievControl = new Controlers.achievementController();
            InitializeComponent();
            List<Models.user_achievement> user_achiev = achievControl.find_achievements();
            achievStatus1.Text = user_achiev[0].status;
            achievStatus2.Text = user_achiev[1].status;
            achievStatus3.Text = user_achiev[2].status;
            achievStatus4.Text = user_achiev[3].status;
            achievStatus5.Text = user_achiev[4].status;
            achievStatus6.Text = user_achiev[5].status;
        }

        private void achiev_box1_Click(object sender, EventArgs e)
        {
            achievement_popup_page info_Page = new achievement_popup_page();
            info_Page.Show();
            info_Page.set_data("Achievement1",achievStatus1.Text, "To unlock this achievement you have to score atleast 1% on any test", 1, sessionInfo.UserID);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            achievement_popup_page info_Page = new achievement_popup_page();
            info_Page.Show();
            info_Page.set_data("Achievement2", achievStatus2.Text, "To unlock this achievement you have to score atleast 20% on any test", 2, sessionInfo.UserID);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            achievement_popup_page info_Page = new achievement_popup_page();
            info_Page.Show();
            info_Page.set_data("Achievement3", achievStatus3.Text, "To unlock this achievement you have to score atleast 40% on any test", 3, sessionInfo.UserID);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            achievement_popup_page info_Page = new achievement_popup_page();
            info_Page.Show();
            info_Page.set_data("Achievement4", achievStatus4.Text, "To unlock this achievement you have to score atleast 60% on any test", 4, sessionInfo.UserID);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            achievement_popup_page info_Page = new achievement_popup_page();
            info_Page.Show();
            info_Page.set_data("Achievement5", achievStatus5.Text, "To unlock this achievement you have to score atleast 80% on any test", 5, sessionInfo.UserID);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            achievement_popup_page info_Page = new achievement_popup_page();
            info_Page.Show();
            info_Page.set_data("Achievement6", achievStatus6.Text, "To unlock this achievement you have to score atleast 100% on any test", 6, sessionInfo.UserID);
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            home_page home_Page = new home_page();
            home_Page.Show();
        }
    }
}
