﻿namespace Egzaminatorius.Views
{
    partial class comment_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Commentbox = new System.Windows.Forms.RichTextBox();
            this.CommentSubmit = new System.Windows.Forms.Button();
            this.CommentLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Commentbox
            // 
            this.Commentbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Commentbox.Location = new System.Drawing.Point(250, 57);
            this.Commentbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Commentbox.Name = "Commentbox";
            this.Commentbox.Size = new System.Drawing.Size(600, 299);
            this.Commentbox.TabIndex = 1;
            this.Commentbox.Text = "";
            this.Commentbox.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // CommentSubmit
            // 
            this.CommentSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.CommentSubmit.Location = new System.Drawing.Point(447, 373);
            this.CommentSubmit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CommentSubmit.Name = "CommentSubmit";
            this.CommentSubmit.Size = new System.Drawing.Size(200, 50);
            this.CommentSubmit.TabIndex = 2;
            this.CommentSubmit.Text = "Išsaugoti";
            this.CommentSubmit.UseVisualStyleBackColor = true;
            this.CommentSubmit.Click += new System.EventHandler(this.FillForm);
            // 
            // CommentLabel
            // 
            this.CommentLabel.AutoSize = true;
            this.CommentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.CommentLabel.Location = new System.Drawing.Point(352, 9);
            this.CommentLabel.Name = "CommentLabel";
            this.CommentLabel.Size = new System.Drawing.Size(206, 25);
            this.CommentLabel.TabIndex = 3;
            this.CommentLabel.Text = "Parašykite komentarą:";
            // 
            // comment_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Controls.Add(this.CommentLabel);
            this.Controls.Add(this.CommentSubmit);
            this.Controls.Add(this.Commentbox);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "comment_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Komentarų forma";
            this.Load += new System.EventHandler(this.CommentsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox Commentbox;
        private System.Windows.Forms.Button CommentSubmit;
        private System.Windows.Forms.Label CommentLabel;
    }
}