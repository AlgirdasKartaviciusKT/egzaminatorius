﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class test_page : Form
    {
        public test_page()
        {
            InitializeComponent();
        }

        private void test_page_Load(object sender, EventArgs e)
        {
            int testId = Views.test_list_page.testId;
            Models.Test test = new Models.Test();
            Models.Test info = test.getTestInfo(testId);

            string testInfo = String.Format("( " + info.name + " )\n" +
                "Sudėtingumas: " + info.complexity + "\n" +
                "Sprendimui skirtas laikas: " + info.time + " min.");
            richTextBox1.Text = testInfo;
        }

        // grįžimo atgal mygtukas
        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Views.test_list_page test_List_Page = new Views.test_list_page();
            test_List_Page.Show();
        }

        // atvaizduojama testo informacija
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkTestResult_Click(object sender, EventArgs e)
        {
            int testId = Views.test_list_page.testId;
            Controlers.test_controller testCtrl = new Controlers.test_controller();
            List<Models.UserTest> info = testCtrl.openTestResult(testId, 1); // arg - testId, userId
            List<string> result = new List<string>();

            if (info != null && info.Any())
            {
                foreach (var item in info)
                {
                    result.Add("Būsena: " + item.status + "\t Taškų sk.: " + item.points);
                }
            } else
            {
                result.Add("Jūs dar nesate laikęs šio testo");
            }
            listBox1.DataSource = result;
            listBox1.Show();
        }

        private void showTUL_Click(object sender, EventArgs e)
        {
            int testId = Views.test_list_page.testId;
            Controlers.test_controller testCtrl = new Controlers.test_controller();
            List<Models.User> testUsers = testCtrl.openTestUserList(testId);
            List<string> testsList = new List<string>();

            if (testUsers != null && testUsers.Any())
            {
                foreach (var item in testUsers)
                {
                    testsList.Add(item.name +"\t" + item.surname);
                }
                listBox1.DataSource = testsList;
            }
            else
            {
                testsList.Add("Šio testo dar niekas nėra laikęs.");
            }
            listBox1.DataSource = testsList;
            listBox1.Show();
        }

        private void solveTest_Click(object sender, EventArgs e)
        {
            this.Hide();
            Controlers.test_controller testCtrl = new Controlers.test_controller();
            testCtrl.openTest();
        }
    }
}
