﻿namespace Egzaminatorius.Views
{
    partial class article_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.article_comment = new System.Windows.Forms.Button();
            this.article_reviewComments = new System.Windows.Forms.Button();
            this.LikeArticle = new System.Windows.Forms.Button();
            this.DislikeArticle = new System.Windows.Forms.Button();
            this.likesLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.approve_button = new System.Windows.Forms.Button();
            this.reject_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // article_comment
            // 
            this.article_comment.Location = new System.Drawing.Point(739, 640);
            this.article_comment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.article_comment.Name = "article_comment";
            this.article_comment.Size = new System.Drawing.Size(108, 36);
            this.article_comment.TabIndex = 0;
            this.article_comment.Text = "Komentuoti";
            this.article_comment.UseVisualStyleBackColor = true;
            this.article_comment.Click += new System.EventHandler(this.addComment);
            // 
            // article_reviewComments
            // 
            this.article_reviewComments.Location = new System.Drawing.Point(865, 640);
            this.article_reviewComments.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.article_reviewComments.Name = "article_reviewComments";
            this.article_reviewComments.Size = new System.Drawing.Size(115, 36);
            this.article_reviewComments.TabIndex = 1;
            this.article_reviewComments.Text = "Komentarai";
            this.article_reviewComments.UseVisualStyleBackColor = true;
            this.article_reviewComments.Click += new System.EventHandler(this.chooseComments);
            // 
            // LikeArticle
            // 
            this.LikeArticle.Location = new System.Drawing.Point(120, 640);
            this.LikeArticle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LikeArticle.Name = "LikeArticle";
            this.LikeArticle.Size = new System.Drawing.Size(75, 36);
            this.LikeArticle.TabIndex = 3;
            this.LikeArticle.Text = "Patiko";
            this.LikeArticle.UseVisualStyleBackColor = true;
            this.LikeArticle.Click += new System.EventHandler(this.LikeArticle_Click);
            // 
            // DislikeArticle
            // 
            this.DislikeArticle.Location = new System.Drawing.Point(276, 640);
            this.DislikeArticle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DislikeArticle.Name = "DislikeArticle";
            this.DislikeArticle.Size = new System.Drawing.Size(91, 36);
            this.DislikeArticle.TabIndex = 4;
            this.DislikeArticle.Text = "Nepatiko";
            this.DislikeArticle.UseVisualStyleBackColor = true;
            this.DislikeArticle.Click += new System.EventHandler(this.DislikeArticle_Click);
            // 
            // likesLabel
            // 
            this.likesLabel.AutoSize = true;
            this.likesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.likesLabel.Location = new System.Drawing.Point(61, 613);
            this.likesLabel.Name = "likesLabel";
            this.likesLabel.Size = new System.Drawing.Size(150, 20);
            this.likesLabel.TabIndex = 8;
            this.likesLabel.Text = "Įvertinkite straipsnį";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(67, 49);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 27);
            this.button1.TabIndex = 10;
            this.button1.Text = "<-";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.PreviousPage);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(53, 645);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.Location = new System.Drawing.Point(208, 645);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "label2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.Location = new System.Drawing.Point(60, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 31);
            this.label3.TabIndex = 13;
            this.label3.Text = "label3";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.richTextBox1.Location = new System.Drawing.Point(67, 133);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(913, 476);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // approve_button
            // 
            this.approve_button.Location = new System.Drawing.Point(761, 50);
            this.approve_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.approve_button.Name = "approve_button";
            this.approve_button.Size = new System.Drawing.Size(100, 28);
            this.approve_button.TabIndex = 15;
            this.approve_button.Text = "Patvirtinti";
            this.approve_button.UseVisualStyleBackColor = true;
            this.approve_button.Visible = false;
            this.approve_button.Click += new System.EventHandler(this.approve_button_Click);
            // 
            // reject_button
            // 
            this.reject_button.Location = new System.Drawing.Point(871, 50);
            this.reject_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.reject_button.Name = "reject_button";
            this.reject_button.Size = new System.Drawing.Size(100, 28);
            this.reject_button.TabIndex = 16;
            this.reject_button.Text = "Atmesti";
            this.reject_button.UseVisualStyleBackColor = true;
            this.reject_button.Visible = false;
            this.reject_button.Click += new System.EventHandler(this.reject_button_Click);
            // 
            // article_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Controls.Add(this.reject_button);
            this.Controls.Add(this.approve_button);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.likesLabel);
            this.Controls.Add(this.DislikeArticle);
            this.Controls.Add(this.LikeArticle);
            this.Controls.Add(this.article_reviewComments);
            this.Controls.Add(this.article_comment);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "article_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "article_page";
            this.Load += new System.EventHandler(this.article_page_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button article_comment;
        private System.Windows.Forms.Button article_reviewComments;
        private System.Windows.Forms.Button LikeArticle;
        private System.Windows.Forms.Button DislikeArticle;
        private System.Windows.Forms.Label likesLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button approve_button;
        private System.Windows.Forms.Button reject_button;
    }
}