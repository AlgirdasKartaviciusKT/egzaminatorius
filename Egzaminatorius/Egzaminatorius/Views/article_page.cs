﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egzaminatorius.Models;
using Egzaminatorius.Controlers;

namespace Egzaminatorius.Views
{
    public partial class article_page : Form
    {
        private Article article;
        articleController articleController = new articleController();
        private bool adminMode = false;
        
        public article_page(Article article, articleController ac, bool adminMode)
        {
            this.article = article;
            this.articleController = ac;
            this.adminMode = adminMode;
            InitializeComponent();
        }

        public article_page(Article article,  bool adminMode)
        {
            this.article = article;
          
            this.adminMode = adminMode;
            InitializeComponent();
        }

        private void setContent()
        {
            if(adminMode)
            {
                label3.Text = article.headline;
                richTextBox1.Text = article.content;
                label1.Visible = false;
                label2.Visible = false;
                likesLabel.Visible = false;
                LikeArticle.Visible = false;
                DislikeArticle.Visible = false;
                article_comment.Visible = false;
                article_reviewComments.Visible = false;

                approve_button.Visible = true;
                reject_button.Visible = true;
            }
            else
            {
                if (sessionInfo.UserID == -1)
                {
                    likesLabel.Visible = false;
                    LikeArticle.Visible = false;
                    DislikeArticle.Visible = false;
                    article_comment.Visible = false;
                    article_reviewComments.Visible = false;
                }
                label3.Text = article.headline;
                richTextBox1.Text = article.content;
                label1.Text = article.likeCount.ToString();
                label2.Text = article.dislikeCount.ToString();
            }
        }

        private void addComment(object sender, EventArgs e)
        {
            commentsControler comment_Form = new commentsControler();
            comment_Form.openCommentForm(article.articleId);
            this.Hide();
        }

        private void chooseComments(object sender, EventArgs e)
        {
            commentsControler comment_Form = new commentsControler();
            comment_Form.getComment(article.articleId);
            this.Hide();
        }

        private void LikeArticle_Click(object sender, EventArgs e)
        {
            articleController.grade(true, article.articleId);
            article.likeCount++;
            label1.Text = article.likeCount.ToString();
            
        }

        private void DislikeArticle_Click(object sender, EventArgs e)
        {
            articleController.grade(false, article.articleId);
            article.dislikeCount++;
            label2.Text = article.dislikeCount.ToString();
        }

        private void article_page_Load(object sender, EventArgs e)
        {
            setContent();
        }

        private void PreviousPage(object sender, EventArgs e)
        {
            if (!adminMode)
            {
                articleController.getArticleList(article.fkTopic);
            } else
            {
                articleController.getWaitingList();
            }
            this.Hide();
        }

        private void approve_button_Click(object sender, EventArgs e)
        {
            articleController.approveArticle(article);
        }

        private void reject_button_Click(object sender, EventArgs e)
        {

            articleController.rejectArticle(article);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
