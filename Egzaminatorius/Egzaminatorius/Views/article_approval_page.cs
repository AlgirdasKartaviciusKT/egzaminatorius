﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egzaminatorius.Models;
using Egzaminatorius.Controlers;

namespace Egzaminatorius.Views
{
    public partial class article_approval_page : Form
    {
        private List<Article> published;
        private List<Article> articles;
        articleController articleController;


        public article_approval_page(List<Article> articles, List<Article> published, articleController ac)
        {
            this.articles = articles;
            this.published = published;
            this.articleController = ac;
            InitializeComponent();
            loadContents();
        }

        //back button
        private void button1_Click(object sender, EventArgs e)
        {
            home_page home = new home_page();
            home.Show();
            this.Hide();
        }

        private void loadContents()
        {
            listBox1.Items.Clear();
            foreach (Article aa in articles)
            {
                listBox1.Items.Add("[ " + aa.fkTopic + " ]  " + aa.headline);
            }

            listBox2.Items.Clear();
            foreach (Article ap in published)
            {
                listBox2.Items.Add("[ " + ap.fkTopic + " ]  " + ap.headline);
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox1.IndexFromPoint(e.Location);
            string article = listBox1.Items[index].ToString().Split(']')[1].Trim();

            articleController.judgeArticle(article);
        }

        //delete button
        private void button2_Click(object sender, EventArgs e)
        {
            string article = listBox2.SelectedItem.ToString().Split(']')[1].Trim();
            articleController.removeArticle(article);
            this.Hide();
        }
    }
}
