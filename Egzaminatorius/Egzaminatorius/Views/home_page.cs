﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egzaminatorius.Controlers;

namespace Egzaminatorius.Views
{
    public partial class home_page : Form
    {
        userController userControl;
        public home_page()
        {
            InitializeComponent();
            userControl = new userController();
            Models.User userData = userControl.edit_user();
            nameSurnameLabel.Text = userData.name + " " + userData.surname;
            powerLabel.Text = sessionInfo.userRole;
            if (sessionInfo.userRole != "admin")
            {
                searchButton.Hide();
                button2.Visible = false;
            }
            if (sessionInfo.userRole == "guest")
            {
                editButton.Visible = false;
            }
        }

        private void getCategoryList(object sender, EventArgs e)
        {
            categoryController.openCategoryList();
            this.Hide();
        }


        private void openTestList_Click(object sender, EventArgs e)
        {
            this.Hide();
            Controlers.test_controller testCtrl = new Controlers.test_controller();
            testCtrl.openTestList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            articleController.getWaitingList();
            this.Hide();
        }

        private void home_page_Load(object sender, EventArgs e)
        {

        }

        private void logout()
        {
            Controlers.userController userControl = new Controlers.userController();
            if (sessionInfo.UserID != -1)
                userControl.logout();
            this.Hide();
            login_page loginPage = new login_page();
            loginPage.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            logout();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            edit_user();
        }

        private void edit_user()
        {
            this.Hide();
            user_edit_page edit_Page = new user_edit_page();
            edit_Page.Show();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            user_search_page search_Page = new user_search_page();
            search_Page.Show();
        }

        private void achievButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            achievement_page achiev_Page = new achievement_page();
            achiev_Page.Show();
        }
    }
}
