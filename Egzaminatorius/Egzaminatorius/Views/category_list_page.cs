﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egzaminatorius.Models;
using Egzaminatorius.Controlers;

namespace Egzaminatorius.Views
{
    public partial class category_list_page : Form
    {

        private List<topic> topics;
        private categoryController controller;

        public category_list_page(List<topic> t, categoryController cc)
        {
            this.topics = t;
            this.controller = cc;
            InitializeComponent();
            if(sessionInfo.userRole != "admin")
            {
                button2.Visible = false;
            }
        }

        //grizti atgal mygtukas
        private void button1_Click(object sender, EventArgs e)
        {
            home_page home = new home_page();
            home.Show();
            this.Hide();
        }

        //naujos srities mygtukas
        private void button2_Click(object sender, EventArgs e)
        {
            this.openNewCategoryPage();
        }

        //naujos srities kurimo mygtukas
        private void button3_Click(object sender, EventArgs e)
        {
            string new_name = textBox1.Text;
            if(controller.createArticle(new_name))
            {
                log("Nauja sritis sukurta sėkmingai", false);
                listBox1.Items.Add(new_name);
            }
            else
            {
                log("Prašome pateikti neegzistuojantį pavadinimą 3-40 simbolių ilgio", true);
            }
        }

        public void openNewCategoryPage()
        {
            textBox1.Visible = !textBox1.Visible;
            button3.Visible = !button3.Visible;
        }

        public void show()
        {

        }

        public void getCategoryArticleList(string category)
        {
            articleController aController = new articleController();
            aController.getArticleList(category);
            this.Hide();
        }

        public void editArticle()
        {

        }

        private void category_list_page_Load(object sender, EventArgs e)
        {
            listCategories();
        }

        private void listCategories()
        {
            listBox1.Items.Clear();
            foreach (topic tt in topics)
            {
                listBox1.Items.Add(tt.name);
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox1.IndexFromPoint(e.Location);
            string category = listBox1.Items[index].ToString();
            getCategoryArticleList(category);
        }

        private void log(string msg, bool isError)
        {
            label1.Visible = true;
            if (isError) label1.ForeColor = Color.Red;
            else  label1.ForeColor = Color.Green;
            label1.Text = msg;
        }
    }
}
