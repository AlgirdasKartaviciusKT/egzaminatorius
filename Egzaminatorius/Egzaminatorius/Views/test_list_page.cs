﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class test_list_page : Form
    {
        // kintamasis reikalingas perduoti testo id kitai formai
        public static int testId;

        public test_list_page()
        {
            InitializeComponent();
        }

        // grįžimo mygtukas į pagr. puslapį
        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Views.home_page home_Page = new Views.home_page();
            home_Page.Show();
        }

        // testų sąrašo forma
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // užkraunamas testų sąrašas
        private void test_list_page_Load(object sender, EventArgs e)
        {
            loadTests();
        }

        private void showTest_Click(object sender, EventArgs e)
        {
            string selected = listBox1.SelectedItem.ToString();
            string[] values = selected.Split('.');
            testId = int.Parse(values[0]);
            Controlers.test_controller testCtrl = new Controlers.test_controller();
            testCtrl.openTestInfo();
            this.Hide();
        }

        private void createTest_Click(object sender, EventArgs e)
        {
            Controlers.test_controller testCtrl = new Controlers.test_controller();
            testCtrl.openTestForm();
            this.Hide();
        }

        private void editTest_Click(object sender, EventArgs e)
        {

        }

        private void delete_Click(object sender, EventArgs e)
        {
            string selected = listBox1.SelectedItem.ToString();
            string[] values = selected.Split('.');
            testId = int.Parse(values[0]);

            Controlers.test_controller testCtrl = new Controlers.test_controller();
            testCtrl.deleteTest(testId);

            loadTests();
        }

        public void loadTests()
        {
            Models.Test test = new Models.Test();
            List<Models.Test> selectTests = test.selectTests();
            List<string> testsList = new List<string>();

            foreach (var item in selectTests)
            {
                testsList.Add(item.ToString());
            }
            listBox1.DataSource = testsList;
        }
    }
}
