﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class comment_page : Form
        
    {
        private int articleId = -1;
        private string articleContent = "";
        public comment_page(int id)
        {
            this.articleId = id;
           
            InitializeComponent();
        }
        public comment_page(int id, string text)
        {
            this.articleId = id;
            this.articleContent = text;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGrid1_Navigate(object sender, NavigateEventArgs ne)
        {
            
          
            
        }

        private void CommentList_Load(object sender, EventArgs e)
        {
            Comments.Hide();
            List<string[]> list = new List<string[]>();
            Models.Comment comments = new Models.Comment();
            List<Models.Comment> commentsList = comments.SelectComments(articleId);
            StringBuilder sb = new StringBuilder();
            List<string> g = new List<string>();
            
           
            foreach (var item in commentsList)
            {            
               
                g.Add(format(item));

            }
            listBox1.DataSource = g;



            /* this.listBox1.Items.AddRange(new object[] {
             "Item 1, column 1",
             "Item 2, column 1",
             "Item 3, column 1",
             "Item 4, column 1",
             "Item 5, column 1",
             "Item 1, column 2",
             "Item 2, column 2",
             "Item 3, column 2"});*/
            if (sessionInfo.userRole != "admin")
            {
                delete.Visible = false;
            }

                this.listBox1.Name = "listBox1";
     
            
            this.listBox1.TabIndex = 0;
            this.listBox1.ColumnWidth = 150;
            this.Controls.Add(this.listBox1);

            // Convert to DataTable.
            // DataTable table = ConvertListToDataTable(list);

            // dataGrid1.DataSource = table;
        }
        // Example list.
        
        

        static DataTable ConvertListToDataTable(List<string []> list)
{
    // New table.
    DataTable table = new DataTable();

            int columns = 4;
            

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
}

        private void CommentsList_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
          


        }

        private void deleteComment(object sender, EventArgs e)
        {

            string line = listBox1.SelectedItem.ToString();
            string[] lines = line.Split(' ');
            int index = int.Parse(lines[0]);
            Controlers.commentsControler commentsControler = new Controlers.commentsControler();
            commentsControler.delete(index);

            Models.Comment comment = new Models.Comment();
            List<Models.Comment> commentsList = comment.SelectComments(articleId);

            List<string> g = new List<string>();


           
            foreach (var item in commentsList)
            {

                g.Add(format(item));

            }
            listBox1.DataSource = g;


            listBox1.DataSource = null;
            listBox1.DataSource = g;
              Comments.Hide();
           
              

        }

        private void Like(object sender, EventArgs e)
        {
            string line = listBox1.SelectedItem.ToString();
            string[] lines = line.Split(' ');
            int index = int.Parse(lines[0]);
            Controlers.commentsControler commentsControler = new Controlers.commentsControler();
            commentsControler.rate(true, index);
            Models.Comment comment = new Models.Comment();
            List<Models.Comment> commentsList = comment.SelectComments(articleId);

            List<string> g = new List<string>();

            
            foreach (var item in commentsList)
            {

                g.Add(format(item));

            }
            listBox1.DataSource = g;


            listBox1.DataSource = null;
            listBox1.DataSource = g;

        }

        private void Dislike(object sender, EventArgs e)
        {

            string line = listBox1.SelectedItem.ToString();
            string[] lines = line.Split(' ');
            int index = int.Parse(lines[0]);
            Controlers.commentsControler commentsControler = new Controlers.commentsControler();
            Models.Comment comment = new Models.Comment();
            commentsControler.rate(false, index);
           List<Models.Comment> commentsList = comment.SelectComments(articleId);

            List<string> g = new List<string>();


          
            foreach (var item in commentsList)
            {

                g.Add(format(item));

            }
            listBox1.DataSource = g;


            listBox1.DataSource = null;
            listBox1.DataSource = g;

        }

        private void review(object sender, EventArgs e)
        {
            Comments.Show();
            string line = listBox1.SelectedItem.ToString();
            string[] lines = line.Split(' ');
            int index = int.Parse(lines[0]);
            Controlers.commentsControler commentsControler = new Controlers.commentsControler();
            string text = commentsControler.review(index);          

            StringBuilder sb = new StringBuilder();
            sb.Append(text);
            Comments.Text = sb.ToString();
        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Controlers.commentsControler commentsControler = new Controlers.commentsControler();
         
            commentsControler.back(articleId);
          
        }
        private string format(Models.Comment comment)
        {
            string text = String.Format(comment.commentId + " |{0,5}|{1,8}|{2}" , comment.like, comment.dislike, comment.content);
            return text;
        }
    }
}
  

