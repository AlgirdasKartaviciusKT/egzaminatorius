﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class achievement_popup_page : Form
    {
        int achiev_id = -1;
        int user_id = -1;
        public achievement_popup_page()
        {
            InitializeComponent();
        }

        public void set_data(string name, string status, string descr, int a_id, int u_id)
        {
            if (status == "locked")
                displayButton.Hide();
            achievName.Text = name;
            achievStatus.Text = status;
            achievDescr.Text = descr;
            achiev_id = a_id;
            user_id = u_id;
        }

        public void mark_achievement()
        {
            Controlers.achievementController achievControl = new Controlers.achievementController();
            achievControl.mark_achievement(achiev_id, user_id);
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            mark_achievement();
        }
    }
}
