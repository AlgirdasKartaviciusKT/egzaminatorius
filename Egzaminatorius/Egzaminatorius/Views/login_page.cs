﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class login_page : Form
    {
        Controlers.userController userControl;
        home_page home_Page;
        register_page reg_Page;

        public login_page()
        {
            InitializeComponent();
            loginButton.Select();
            loginButton.Select();
            userControl = new Controlers.userController();
            reg_Page = new register_page();
            passwordBox.PasswordChar = '\0';
        }


public void submit_login()
        {
            errorBox.Hide();
            usernameBox.BackColor = Color.White;
            passwordBox.BackColor = Color.White;
            string username = usernameBox.Text;
            string password = passwordBox.Text;
            if (userControl.check_session_id())
            {
                this.Hide();
                home_Page = new home_page();
                home_Page.Show();
            }
            else
            {
                if (username != "Username" && password != "Password")
                {
                    int retIndex = userControl.validate_log(username, password);
                    if (retIndex == 0)
                    {
                        this.Hide();
                        home_Page = new home_page();
                        home_Page.Show();
                    }
                    else if (retIndex == 1)
                    {
                        errorBox.Show();
                        errorBox.Text = "Username/Password don't match";
                    }
                }
                else
                    missingFieldsError(username, password);
            }
        }

        public void missingFieldsError(string username, string password)
        {
            if (username == "Username")
                usernameBox.BackColor = Color.Red;

            if (password == "Password")
                passwordBox.BackColor = Color.Red;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            submit_login();
        }

        private void usernameBox_Enter(object sender, EventArgs e)
        {
            if (usernameBox.Text == "Username")
            {
                usernameBox.Text = "";
            }
        }

        private void usernameBox_Leave(object sender, EventArgs e)
        {
            if (usernameBox.Text == "")
            {
                usernameBox.Text = "Username";
            }
        }

        private void passwordBox_Enter(object sender, EventArgs e)
        {
            if (passwordBox.Text == "Password")
            {
                passwordBox.PasswordChar = '*';
                passwordBox.Text = "";
            }
        }

        private void passwordBox_Leave(object sender, EventArgs e)
        {
            if (passwordBox.Text == "")
            {
                passwordBox.PasswordChar = '\0';
                passwordBox.Text = "Password";
            }
        }

        private void login_page_Load(object sender, EventArgs e)
        {

        }

        private void registerLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            register();
        }

        private void register()
        {
            this.Hide();
            if (userControl.check_session_id())
            {
                home_Page = new home_page();
                home_Page.Show();
            }
            else
                reg_Page.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sessionInfo.UserID = 1;
            sessionInfo.userRole = "member";
            this.Hide();
            home_Page = new home_page();
            home_Page.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sessionInfo.UserID = 2;
            sessionInfo.userRole = "admin";
            this.Hide();
            home_Page = new home_page();
            home_Page.Show();
        }

        private void guestButton_Click(object sender, EventArgs e)
        {
            sessionInfo.UserID = -1;
            sessionInfo.userRole = "guest";
            this.Hide();
            home_Page = new home_page();
            home_Page.Show();
        }
    }
}
