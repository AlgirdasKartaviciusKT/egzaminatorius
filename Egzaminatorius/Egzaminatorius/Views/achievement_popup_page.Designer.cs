﻿namespace Egzaminatorius.Views
{
    partial class achievement_popup_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.achievStatus = new System.Windows.Forms.Label();
            this.achievName = new System.Windows.Forms.Label();
            this.achiev_box = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.achievDescr = new System.Windows.Forms.Label();
            this.displayButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.achiev_box)).BeginInit();
            this.SuspendLayout();
            // 
            // achievStatus
            // 
            this.achievStatus.AutoSize = true;
            this.achievStatus.Location = new System.Drawing.Point(303, 67);
            this.achievStatus.Name = "achievStatus";
            this.achievStatus.Size = new System.Drawing.Size(43, 13);
            this.achievStatus.TabIndex = 44;
            this.achievStatus.Text = "Locked";
            // 
            // achievName
            // 
            this.achievName.AutoSize = true;
            this.achievName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.achievName.Location = new System.Drawing.Point(238, 42);
            this.achievName.Name = "achievName";
            this.achievName.Size = new System.Drawing.Size(137, 25);
            this.achievName.TabIndex = 43;
            this.achievName.Text = "Achievement1";
            // 
            // achiev_box
            // 
            this.achiev_box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.achiev_box.Location = new System.Drawing.Point(61, 42);
            this.achiev_box.Name = "achiev_box";
            this.achiev_box.Size = new System.Drawing.Size(100, 100);
            this.achiev_box.TabIndex = 42;
            this.achiev_box.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(254, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Status   -";
            // 
            // achievDescr
            // 
            this.achievDescr.Location = new System.Drawing.Point(180, 89);
            this.achievDescr.Name = "achievDescr";
            this.achievDescr.Size = new System.Drawing.Size(243, 53);
            this.achievDescr.TabIndex = 46;
            this.achievDescr.Text = "To unlock this achievement you have to score atleast 50% on any test";
            this.achievDescr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // displayButton
            // 
            this.displayButton.Location = new System.Drawing.Point(225, 145);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(150, 23);
            this.displayButton.TabIndex = 47;
            this.displayButton.Text = "Display this achievement";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // achievement_popup_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 206);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.achievDescr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.achievStatus);
            this.Controls.Add(this.achievName);
            this.Controls.Add(this.achiev_box);
            this.Name = "achievement_popup_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Egzaminatorius - Achievement info";
            ((System.ComponentModel.ISupportInitialize)(this.achiev_box)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label achievStatus;
        private System.Windows.Forms.Label achievName;
        private System.Windows.Forms.PictureBox achiev_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label achievDescr;
        private System.Windows.Forms.Button displayButton;
    }
}