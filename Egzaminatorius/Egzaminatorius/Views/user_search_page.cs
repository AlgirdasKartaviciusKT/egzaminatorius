﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class user_search_page : Form
    {
        Controlers.userController userControl;

        public user_search_page()
        {
            InitializeComponent();
            userControl = new Controlers.userController();
            var button = new DataGridViewButtonColumn();
            button.Name = "blockButton";
            button.HeaderText = "Block";
            button.Text = "Block";
            button.UseColumnTextForButtonValue = true;
            this.dataGridView1.Columns.Add(button);
        }

        public void submit_search()
        {
            List<Models.User> users = userControl.search_for_users(searchBox.Text);
            dataGridView1.DataSource = users;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                block(dataGridView1[1, e.RowIndex].Value, dataGridView1[0, e.RowIndex].Value);
            }
        }

        public void block(object username, object id)
        {
            block_page block_Page = new block_page();
            block_Page.Show();
            block_Page.open_block(Convert.ToString(username), Convert.ToInt32(id));
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            home_page home_Page = new home_page();
            home_Page.Show();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            submit_search();
        }
    }
}
