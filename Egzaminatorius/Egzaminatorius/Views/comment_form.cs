﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class comment_form : Form
    {
        private int articleId = -1;
        public comment_form(int id)
        {
            this.articleId = id;
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void CommentsForm_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void FillForm(object sender, EventArgs e)
        {
            Controlers.commentsControler commentsControler = new Controlers.commentsControler();
            string line = Commentbox.Text;
            if (commentsControler.checkData(line)) {

                int fkArticleId = articleId;
                Models.Comment c = new Models.Comment();
               
                Models.Comment cc = new Models.Comment { content = line, fkstraipsnioId = fkArticleId };
                c.InsertComment(cc);
                this.Hide();
                comment_page a = new comment_page(articleId);
                a.Show();
                


            }
            else
            {
                
                MessageBox.Show("Parašykite komentarą");
            }
        }
    }
}
