﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class block_page : Form
    {
        int blockId = -1;
        Controlers.userController userControl;

        public block_page()
        {
            InitializeComponent();
            userControl = new Controlers.userController();
        }

        public void open_block(string username, int id)
        {
            blockId = id;
            usernameLabel.Text = username;
        }

        public void submit_reason()
        {
            userControl.block(blockId);
            this.Hide();
        }

        private void blockButton_Click(object sender, EventArgs e)
        {
            submit_reason();
        }
    }
}
