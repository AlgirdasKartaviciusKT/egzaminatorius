﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egzaminatorius.Models;
using Egzaminatorius.Controlers;

namespace Egzaminatorius.Views
{
    public partial class article_creation_page : Form
    {
        private articleController articleController;
        private string category;
        private Article article;

        public article_creation_page()
        {
            InitializeComponent();
        }

        public article_creation_page(string category, articleController ac)
        {
            this.category = category;
            this.articleController = ac;
            InitializeComponent();
        }

        public article_creation_page(string category, Article a, articleController ac)
        {
            this.category = category;
            this.articleController = ac;
            this.article = a;
            InitializeComponent();
            loadContents();
        }

        private void loadContents()
        {
            textBox1.Text = article.headline;
            richTextBox1.Text = article.content;
        }

        //back button
        private void button1_Click(object sender, EventArgs e)
        {
            articleController.getArticleList(category);
            this.Hide();
        }

        //saugoti button
        private void button2_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Ar norite publikuoti straipsnį?",
                                     "Publikuoti?",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                if(article != null)
                {
                    article.headline = textBox1.Text;
                    article.content = richTextBox1.Text;
                    log(articleController.updateArticle(article, true));
                } else
                {
                    log(articleController.createArticle(textBox1.Text, richTextBox1.Text, category, true));
                }
            }
            else
            {
                if (article != null)
                {
                    article.headline = textBox1.Text;
                    article.content = richTextBox1.Text;
                    log(articleController.updateArticle(article, false));
                }
                else
                {
                    log(articleController.createArticle(textBox1.Text, richTextBox1.Text, category, false));
                }
            }
        }

        private void log(bool success)
        {
            label1.Visible = true;
            if(success)
            {
                label1.ForeColor = Color.Green;
                label1.Text = "Straipsnis išsaugotas";
            }
            else
            {
                label1.ForeColor = Color.Red;
                label1.Text = "Straipsnis neišsaugotas";
            }
        }
    }
}
