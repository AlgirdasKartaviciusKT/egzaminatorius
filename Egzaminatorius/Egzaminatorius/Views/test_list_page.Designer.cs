﻿namespace Egzaminatorius.Views
{
    partial class test_list_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.showTest = new System.Windows.Forms.Button();
            this.createTest = new System.Windows.Forms.Button();
            this.editTest = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(652, 514);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(120, 35);
            this.back.TabIndex = 0;
            this.back.Text = "Grįžti į pagrindinį puslapį";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 23;
            this.listBox1.Location = new System.Drawing.Point(12, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(634, 533);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // showTest
            // 
            this.showTest.Location = new System.Drawing.Point(652, 12);
            this.showTest.Name = "showTest";
            this.showTest.Size = new System.Drawing.Size(120, 35);
            this.showTest.TabIndex = 2;
            this.showTest.Text = "Peržiūrėti testą";
            this.showTest.UseVisualStyleBackColor = true;
            this.showTest.Click += new System.EventHandler(this.showTest_Click);
            // 
            // createTest
            // 
            this.createTest.Location = new System.Drawing.Point(652, 94);
            this.createTest.Name = "createTest";
            this.createTest.Size = new System.Drawing.Size(120, 35);
            this.createTest.TabIndex = 3;
            this.createTest.Text = "Sukurti testą";
            this.createTest.UseVisualStyleBackColor = true;
            this.createTest.Click += new System.EventHandler(this.createTest_Click);
            // 
            // editTest
            // 
            this.editTest.Location = new System.Drawing.Point(652, 53);
            this.editTest.Name = "editTest";
            this.editTest.Size = new System.Drawing.Size(120, 35);
            this.editTest.TabIndex = 4;
            this.editTest.Text = "Redaguoti testą";
            this.editTest.UseVisualStyleBackColor = true;
            this.editTest.Click += new System.EventHandler(this.editTest_Click);
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.SpringGreen;
            this.delete.Location = new System.Drawing.Point(652, 191);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(120, 35);
            this.delete.TabIndex = 5;
            this.delete.Text = "Ištrinti testą";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // test_list_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.editTest);
            this.Controls.Add(this.createTest);
            this.Controls.Add(this.showTest);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.back);
            this.Name = "test_list_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Testų sąrašas";
            this.Load += new System.EventHandler(this.test_list_page_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button showTest;
        private System.Windows.Forms.Button createTest;
        private System.Windows.Forms.Button editTest;
        private System.Windows.Forms.Button delete;
    }
}