﻿namespace Egzaminatorius.Views
{
    partial class home_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.openTestList = new System.Windows.Forms.Button();
            this.article_list_button = new System.Windows.Forms.Button();
            this.timer1 = new System.Timers.Timer();
            this.button2 = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.nameSurnameLabel = new System.Windows.Forms.Label();
            this.powerLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.editButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.achievButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(42, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sritys";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.getCategoryList);
            // 
            // openTestList
            // 
            this.openTestList.Location = new System.Drawing.Point(181, 49);
            this.openTestList.Margin = new System.Windows.Forms.Padding(2);
            this.openTestList.Name = "openTestList";
            this.openTestList.Size = new System.Drawing.Size(120, 33);
            this.openTestList.TabIndex = 1;
            this.openTestList.Text = "Testai";
            this.openTestList.UseVisualStyleBackColor = true;
            this.openTestList.Click += new System.EventHandler(this.openTestList_Click);
            // 
            // article_list_button
            // 
            this.article_list_button.Location = new System.Drawing.Point(834, 366);
            this.article_list_button.Margin = new System.Windows.Forms.Padding(2);
            this.article_list_button.Name = "article_list_button";
            this.article_list_button.Size = new System.Drawing.Size(106, 67);
            this.article_list_button.TabIndex = 1;
            this.article_list_button.Text = "Straipsnių sąrašas";
            this.article_list_button.UseVisualStyleBackColor = true;
            this.article_list_button.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.SynchronizingObject = this;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(42, 504);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 37);
            this.button2.TabIndex = 3;
            this.button2.Text = "Adminerija";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(697, 12);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(75, 23);
            this.logoutButton.TabIndex = 3;
            this.logoutButton.Text = "Log Out";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // nameSurnameLabel
            // 
            this.nameSurnameLabel.Location = new System.Drawing.Point(-1, 0);
            this.nameSurnameLabel.Name = "nameSurnameLabel";
            this.nameSurnameLabel.Size = new System.Drawing.Size(200, 23);
            this.nameSurnameLabel.TabIndex = 4;
            this.nameSurnameLabel.Text = "user name + surname";
            this.nameSurnameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // powerLabel
            // 
            this.powerLabel.Location = new System.Drawing.Point(3, 23);
            this.powerLabel.Name = "powerLabel";
            this.powerLabel.Size = new System.Drawing.Size(200, 23);
            this.powerLabel.TabIndex = 5;
            this.powerLabel.Text = "power level";
            this.powerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.editButton);
            this.panel1.Controls.Add(this.nameSurnameLabel);
            this.panel1.Controls.Add(this.powerLabel);
            this.panel1.Location = new System.Drawing.Point(42, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 215);
            this.panel1.TabIndex = 6;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(68, 187);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 7;
            this.editButton.Text = "Edit profile";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(320, 49);
            this.searchButton.Margin = new System.Windows.Forms.Padding(2);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(117, 33);
            this.searchButton.TabIndex = 7;
            this.searchButton.Text = "Ieškoti vartotojų";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // achievButton
            // 
            this.achievButton.Location = new System.Drawing.Point(452, 49);
            this.achievButton.Margin = new System.Windows.Forms.Padding(2);
            this.achievButton.Name = "achievButton";
            this.achievButton.Size = new System.Drawing.Size(117, 33);
            this.achievButton.TabIndex = 8;
            this.achievButton.Text = "Achievements";
            this.achievButton.UseVisualStyleBackColor = true;
            this.achievButton.Click += new System.EventHandler(this.achievButton_Click);
            // 
            // home_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.achievButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.openTestList);
            this.Controls.Add(this.article_list_button);
            this.Controls.Add(this.button1);
            this.Name = "home_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Egzaminatorius";
            this.Load += new System.EventHandler(this.home_page_Load);
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button openTestList;
        private System.Windows.Forms.Button article_list_button;
        private System.Timers.Timer timer1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Label nameSurnameLabel;
        private System.Windows.Forms.Label powerLabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button achievButton;
    }
}