﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class test_form_page : Form
    {
        public test_form_page()
        {
            InitializeComponent();
        }

        // grįžimo atgal mygtukas
        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Views.test_list_page test_List_Page = new Views.test_list_page();
            test_List_Page.Show();
        }

        // testo sudėtingumo pasirinkimas
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void test_form_page_Load(object sender, EventArgs e)
        {
            complexityBox.DataSource = new List<string> { "-- Pasirinkite --", "Lengvas", "Vidutinis", "Sudėtingas" };
        }

        // testo sukūrimas
        private void createTest_Click(object sender, EventArgs e)
        {
            // testo informacijos kintamieji
            string title = titleBox.Text;
            string cpl = complexityBox.SelectedItem.ToString();
            string solveTime = timeBox.Text;

            // testo klausimo kintamasis
            string question = questionBox.Text;

            // testo atsakymo kintamasis
            string answer = answerBox.Text;

            int sTime = 0; // int priskyrimas
            if (title == "" || cpl == "-- Pasirinkite --" || solveTime == "")
            {
                alert.Visible = true;
                alert.BackColor = Color.Red;
                alert.Text = "Prašome užpildyti visus laukelius!";

                titleBox.BackColor = Color.Red;
                complexityBox.BackColor = Color.Red;
                timeBox.BackColor = Color.Red;
            } else if (!int.TryParse(solveTime, out sTime))
            {
                alert.BackColor = Color.Red;
                timeBox.BackColor = Color.Red;
                alert.Text = "Klaida! Sprendimo laiką prašome įvesti minutėmis (pvz.: 10, 30, 60)!";
            } else
            {
                alert.BackColor = Color.MediumSpringGreen;
                alert.Text = "Sėkmingai sukūrėte testą!";
                titleBox.BackColor = Color.Empty;
                complexityBox.BackColor = Color.Empty;
                timeBox.BackColor = Color.Empty;

                Controlers.test_controller testCtrl = new Controlers.test_controller();

                Models.Test testData = new Models.Test { name = title, complexity = cpl, time = sTime };
                testCtrl.createTest(testData); // įrašoma testo informacija, tada gaunamas id ir perduodamas toliau Q/A

                Models.TestQuestion testQuestions = new Models.TestQuestion { question = question, type = "empty", fk_testId = testData.testId };
                testCtrl.createTest(testQuestions);

                Models.PossibleTestAnswer testAnswers = new Models.PossibleTestAnswer { isCorrect = 1, answer = answer, fk_questionId = testQuestions.questionId };
                testCtrl.createTest(testAnswers);
            }
        }
    }
}
