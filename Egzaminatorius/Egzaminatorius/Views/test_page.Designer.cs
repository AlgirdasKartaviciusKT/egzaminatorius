﻿namespace Egzaminatorius.Views
{
    partial class test_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.solveTest = new System.Windows.Forms.Button();
            this.checkTestResult = new System.Windows.Forms.Button();
            this.showTUL = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(652, 514);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(120, 35);
            this.back.TabIndex = 0;
            this.back.Text = "Grįžti atgal";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(760, 97);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // solveTest
            // 
            this.solveTest.BackColor = System.Drawing.Color.SpringGreen;
            this.solveTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.solveTest.Location = new System.Drawing.Point(335, 99);
            this.solveTest.Name = "solveTest";
            this.solveTest.Size = new System.Drawing.Size(120, 35);
            this.solveTest.TabIndex = 2;
            this.solveTest.Text = "Laikyti testą";
            this.solveTest.UseVisualStyleBackColor = false;
            this.solveTest.Click += new System.EventHandler(this.solveTest_Click);
            // 
            // checkTestResult
            // 
            this.checkTestResult.Location = new System.Drawing.Point(12, 99);
            this.checkTestResult.Name = "checkTestResult";
            this.checkTestResult.Size = new System.Drawing.Size(159, 35);
            this.checkTestResult.TabIndex = 3;
            this.checkTestResult.Text = "Peržiūrėti testo rezultatus";
            this.checkTestResult.UseVisualStyleBackColor = true;
            this.checkTestResult.Click += new System.EventHandler(this.checkTestResult_Click);
            // 
            // showTUL
            // 
            this.showTUL.Location = new System.Drawing.Point(12, 140);
            this.showTUL.Name = "showTUL";
            this.showTUL.Size = new System.Drawing.Size(159, 35);
            this.showTUL.TabIndex = 4;
            this.showTUL.Text = "Peržiūrėti laikiusiųjų sąrašą";
            this.showTUL.UseVisualStyleBackColor = true;
            this.showTUL.Click += new System.EventHandler(this.showTUL_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 23;
            this.listBox1.Location = new System.Drawing.Point(12, 197);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(760, 303);
            this.listBox1.TabIndex = 5;
            this.listBox1.Visible = false;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // test_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.showTUL);
            this.Controls.Add(this.checkTestResult);
            this.Controls.Add(this.solveTest);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.back);
            this.Name = "test_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Testo informacija";
            this.Load += new System.EventHandler(this.test_page_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button solveTest;
        private System.Windows.Forms.Button checkTestResult;
        private System.Windows.Forms.Button showTUL;
        private System.Windows.Forms.ListBox listBox1;
    }
}