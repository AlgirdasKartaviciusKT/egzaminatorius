﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egzaminatorius.Models;
using Egzaminatorius.Controlers;

namespace Egzaminatorius.Views
{
    public partial class article_list_page : Form
    {

        List<Article> articles = new List<Article>();
        List<Article> drafts = new List<Article>();
        articleController controller = new articleController();
        string category;

        public article_list_page(List<Article> t, List<Article> j, articleController cc, string cat)
        {
            this.articles = t;
            this.drafts = j;
            this.controller = cc;
            this.category = cat;
            InitializeComponent();
            label1.Text = category;

            if (sessionInfo.UserID == -1)
            {
                button1.Visible = false;
            }
        }


        private void article_list_page_Load(object sender, EventArgs e)
        {
            listArticles();
        }

        private void listArticles()
        {
            listBox1.Items.Clear();
            foreach (Article aa in articles)
            {
                listBox1.Items.Add(aa.headline);
            }
            listBox2.Items.Clear();
            foreach (Article aa in drafts)
            {
                listBox2.Items.Add(aa.headline);
            }
        }

        //back button
        private void button2_Click(object sender, EventArgs e)
        {
            categoryController.openCategoryList();
            this.Hide();
        }
        

        private void openArticle(object sender, MouseEventArgs e)
        {
            int id = this.listBox1.IndexFromPoint(e.Location);
            string name = listBox1.Items[id].ToString();
            articleController.getArticle(name);
            this.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            controller.openArticleCreation(category);
            this.Hide();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button3.Visible = false;
            if(listBox1.SelectedItem != null)
            {
                string headline = listBox1.SelectedItem.ToString();
                int id = findArticleByHeadline(headline).fkOwnerID;
                if (id == sessionInfo.UserID)
                {
                    button3.Visible = true;
                }
            }
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int id = this.listBox2.IndexFromPoint(e.Location);
            string headline = listBox2.Items[id].ToString();
            articleController.openArticleEdit(findArticleByHeadline(headline));
            this.Hide();
        }

        private Article findArticleByHeadline(string headline)
        {
            Article final = new Article();
            foreach (Article aa in articles)
            {
                if (aa.headline == headline)
                {
                    final = aa;
                    break;
                }
            }
            foreach (Article aa in drafts)
            {
                if (aa.headline == headline)
                {
                    final = aa;
                    break;
                }
            }
            return final;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string headline = listBox1.SelectedItem.ToString();
            articleController.openArticleEdit(findArticleByHeadline(headline));
            this.Hide();
        }
    }
}
