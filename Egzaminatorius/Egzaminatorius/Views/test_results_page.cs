﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class test_results_page : Form
    {
        public test_results_page()
        {
            InitializeComponent();
        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Views.test_page test_Page = new Views.test_page();
            test_Page.Show();
        }

        private void test_results_page_Load(object sender, EventArgs e)
        {
            resultLabel.BackColor = Color.Red;
            resultLabel.Text = "Jūs neišlaikėte testo! Bandykite dar kartą.";
        }
    }
}
