﻿namespace Egzaminatorius.Views
{
    partial class test_form_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Label();
            this.titleBox = new System.Windows.Forms.TextBox();
            this.complexity = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.timeBox = new System.Windows.Forms.TextBox();
            this.time_alert = new System.Windows.Forms.Label();
            this.complexityBox = new System.Windows.Forms.ComboBox();
            this.createTest = new System.Windows.Forms.Button();
            this.alert = new System.Windows.Forms.Label();
            this.questionBox = new System.Windows.Forms.TextBox();
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.AnswerLabel = new System.Windows.Forms.Label();
            this.answerBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(652, 514);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(120, 35);
            this.back.TabIndex = 0;
            this.back.Text = "Grįžti atgal";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.title.Location = new System.Drawing.Point(15, 24);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(99, 23);
            this.title.TabIndex = 1;
            this.title.Text = "Pavadinimas";
            // 
            // titleBox
            // 
            this.titleBox.Location = new System.Drawing.Point(168, 29);
            this.titleBox.Name = "titleBox";
            this.titleBox.Size = new System.Drawing.Size(278, 20);
            this.titleBox.TabIndex = 2;
            // 
            // complexity
            // 
            this.complexity.AutoSize = true;
            this.complexity.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.complexity.Location = new System.Drawing.Point(15, 50);
            this.complexity.Name = "complexity";
            this.complexity.Size = new System.Drawing.Size(110, 23);
            this.complexity.TabIndex = 3;
            this.complexity.Text = "Sudėtingumas";
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.time.Location = new System.Drawing.Point(15, 79);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(130, 23);
            this.time.TabIndex = 5;
            this.time.Text = "Sprendimo laikas";
            // 
            // timeBox
            // 
            this.timeBox.Location = new System.Drawing.Point(168, 82);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(278, 20);
            this.timeBox.TabIndex = 6;
            // 
            // time_alert
            // 
            this.time_alert.AutoSize = true;
            this.time_alert.Location = new System.Drawing.Point(452, 87);
            this.time_alert.Name = "time_alert";
            this.time_alert.Size = new System.Drawing.Size(130, 13);
            this.time_alert.TabIndex = 7;
            this.time_alert.Text = "(Prašome įvesti minutėmis)";
            // 
            // complexityBox
            // 
            this.complexityBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.complexityBox.FormattingEnabled = true;
            this.complexityBox.Location = new System.Drawing.Point(168, 55);
            this.complexityBox.Name = "complexityBox";
            this.complexityBox.Size = new System.Drawing.Size(278, 21);
            this.complexityBox.TabIndex = 9;
            this.complexityBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // createTest
            // 
            this.createTest.Location = new System.Drawing.Point(12, 514);
            this.createTest.Name = "createTest";
            this.createTest.Size = new System.Drawing.Size(120, 35);
            this.createTest.TabIndex = 10;
            this.createTest.Text = "Sukurti testą";
            this.createTest.UseVisualStyleBackColor = true;
            this.createTest.Click += new System.EventHandler(this.createTest_Click);
            // 
            // alert
            // 
            this.alert.AutoSize = true;
            this.alert.Location = new System.Drawing.Point(16, 9);
            this.alert.Name = "alert";
            this.alert.Size = new System.Drawing.Size(28, 13);
            this.alert.TabIndex = 11;
            this.alert.Text = "Alert";
            this.alert.Visible = false;
            // 
            // questionBox
            // 
            this.questionBox.Location = new System.Drawing.Point(94, 177);
            this.questionBox.Name = "questionBox";
            this.questionBox.Size = new System.Drawing.Size(278, 20);
            this.questionBox.TabIndex = 12;
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.AutoSize = true;
            this.QuestionLabel.Location = new System.Drawing.Point(16, 180);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(54, 13);
            this.QuestionLabel.TabIndex = 13;
            this.QuestionLabel.Text = "Klausimas";
            // 
            // AnswerLabel
            // 
            this.AnswerLabel.AutoSize = true;
            this.AnswerLabel.Location = new System.Drawing.Point(16, 211);
            this.AnswerLabel.Name = "AnswerLabel";
            this.AnswerLabel.Size = new System.Drawing.Size(58, 13);
            this.AnswerLabel.TabIndex = 14;
            this.AnswerLabel.Text = "Atsakymas";
            // 
            // answerBox
            // 
            this.answerBox.Location = new System.Drawing.Point(94, 208);
            this.answerBox.Name = "answerBox";
            this.answerBox.Size = new System.Drawing.Size(278, 20);
            this.answerBox.TabIndex = 15;
            // 
            // test_form_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.answerBox);
            this.Controls.Add(this.AnswerLabel);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.questionBox);
            this.Controls.Add(this.alert);
            this.Controls.Add(this.createTest);
            this.Controls.Add(this.complexityBox);
            this.Controls.Add(this.time_alert);
            this.Controls.Add(this.timeBox);
            this.Controls.Add(this.time);
            this.Controls.Add(this.complexity);
            this.Controls.Add(this.titleBox);
            this.Controls.Add(this.title);
            this.Controls.Add(this.back);
            this.Name = "test_form_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Testo sukūrimas/atnaujinimas";
            this.Load += new System.EventHandler(this.test_form_page_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TextBox titleBox;
        private System.Windows.Forms.Label complexity;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.TextBox timeBox;
        private System.Windows.Forms.Label time_alert;
        private System.Windows.Forms.ComboBox complexityBox;
        private System.Windows.Forms.Button createTest;
        private System.Windows.Forms.Label alert;
        private System.Windows.Forms.TextBox questionBox;
        private System.Windows.Forms.Label QuestionLabel;
        private System.Windows.Forms.Label AnswerLabel;
        private System.Windows.Forms.TextBox answerBox;
    }
}