﻿namespace Egzaminatorius.Views
{
    partial class achievement_page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.achiev_box1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.homeButton = new System.Windows.Forms.Button();
            this.achievName1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.achievStatus1 = new System.Windows.Forms.Label();
            this.achievStatus2 = new System.Windows.Forms.Label();
            this.achievStatus3 = new System.Windows.Forms.Label();
            this.achievStatus4 = new System.Windows.Forms.Label();
            this.achievStatus5 = new System.Windows.Forms.Label();
            this.achievStatus6 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.achiev_box1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // achiev_box1
            // 
            this.achiev_box1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.achiev_box1.Location = new System.Drawing.Point(126, 165);
            this.achiev_box1.Name = "achiev_box1";
            this.achiev_box1.Size = new System.Drawing.Size(100, 100);
            this.achiev_box1.TabIndex = 0;
            this.achiev_box1.TabStop = false;
            this.achiev_box1.Click += new System.EventHandler(this.achiev_box1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox1.Location = new System.Drawing.Point(330, 165);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox2.Location = new System.Drawing.Point(526, 165);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 100);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox3.Location = new System.Drawing.Point(126, 341);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 100);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox4.Location = new System.Drawing.Point(330, 341);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 100);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox5.Location = new System.Drawing.Point(526, 341);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 100);
            this.pictureBox5.TabIndex = 5;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // homeButton
            // 
            this.homeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.homeButton.Location = new System.Drawing.Point(667, 12);
            this.homeButton.Name = "homeButton";
            this.homeButton.Size = new System.Drawing.Size(105, 35);
            this.homeButton.TabIndex = 34;
            this.homeButton.Text = "Back";
            this.homeButton.UseVisualStyleBackColor = true;
            this.homeButton.Click += new System.EventHandler(this.homeButton_Click);
            // 
            // achievName1
            // 
            this.achievName1.AutoSize = true;
            this.achievName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.achievName1.Location = new System.Drawing.Point(109, 137);
            this.achievName1.Name = "achievName1";
            this.achievName1.Size = new System.Drawing.Size(137, 25);
            this.achievName1.TabIndex = 35;
            this.achievName1.Text = "Achievement1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(311, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 25);
            this.label1.TabIndex = 36;
            this.label1.Text = "Achievement2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.Location = new System.Drawing.Point(506, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 25);
            this.label2.TabIndex = 37;
            this.label2.Text = "Achievement3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.Location = new System.Drawing.Point(109, 313);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 25);
            this.label3.TabIndex = 38;
            this.label3.Text = "Achievement4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label4.Location = new System.Drawing.Point(311, 313);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 25);
            this.label4.TabIndex = 39;
            this.label4.Text = "Achievement5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label5.Location = new System.Drawing.Point(506, 313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 25);
            this.label5.TabIndex = 40;
            this.label5.Text = "Achievement6";
            // 
            // achievStatus1
            // 
            this.achievStatus1.AutoSize = true;
            this.achievStatus1.Location = new System.Drawing.Point(156, 268);
            this.achievStatus1.Name = "achievStatus1";
            this.achievStatus1.Size = new System.Drawing.Size(43, 13);
            this.achievStatus1.TabIndex = 41;
            this.achievStatus1.Text = "Locked";
            // 
            // achievStatus2
            // 
            this.achievStatus2.AutoSize = true;
            this.achievStatus2.Location = new System.Drawing.Point(357, 268);
            this.achievStatus2.Name = "achievStatus2";
            this.achievStatus2.Size = new System.Drawing.Size(43, 13);
            this.achievStatus2.TabIndex = 42;
            this.achievStatus2.Text = "Locked";
            // 
            // achievStatus3
            // 
            this.achievStatus3.AutoSize = true;
            this.achievStatus3.Location = new System.Drawing.Point(556, 268);
            this.achievStatus3.Name = "achievStatus3";
            this.achievStatus3.Size = new System.Drawing.Size(43, 13);
            this.achievStatus3.TabIndex = 43;
            this.achievStatus3.Text = "Locked";
            // 
            // achievStatus4
            // 
            this.achievStatus4.AutoSize = true;
            this.achievStatus4.Location = new System.Drawing.Point(156, 444);
            this.achievStatus4.Name = "achievStatus4";
            this.achievStatus4.Size = new System.Drawing.Size(43, 13);
            this.achievStatus4.TabIndex = 44;
            this.achievStatus4.Text = "Locked";
            // 
            // achievStatus5
            // 
            this.achievStatus5.AutoSize = true;
            this.achievStatus5.Location = new System.Drawing.Point(357, 444);
            this.achievStatus5.Name = "achievStatus5";
            this.achievStatus5.Size = new System.Drawing.Size(43, 13);
            this.achievStatus5.TabIndex = 45;
            this.achievStatus5.Text = "Locked";
            // 
            // achievStatus6
            // 
            this.achievStatus6.AutoSize = true;
            this.achievStatus6.Location = new System.Drawing.Point(556, 444);
            this.achievStatus6.Name = "achievStatus6";
            this.achievStatus6.Size = new System.Drawing.Size(43, 13);
            this.achievStatus6.TabIndex = 46;
            this.achievStatus6.Text = "Locked";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label6.Location = new System.Drawing.Point(241, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(272, 38);
            this.label6.TabIndex = 47;
            this.label6.Text = "My achievements";
            // 
            // achievement_page
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.achievStatus6);
            this.Controls.Add(this.achievStatus5);
            this.Controls.Add(this.achievStatus4);
            this.Controls.Add(this.achievStatus3);
            this.Controls.Add(this.achievStatus2);
            this.Controls.Add(this.achievStatus1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.achievName1);
            this.Controls.Add(this.homeButton);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.achiev_box1);
            this.Name = "achievement_page";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Egzaminatorius - Achievements";
            ((System.ComponentModel.ISupportInitialize)(this.achiev_box1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox achiev_box1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button homeButton;
        private System.Windows.Forms.Label achievName1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label achievStatus1;
        private System.Windows.Forms.Label achievStatus2;
        private System.Windows.Forms.Label achievStatus3;
        private System.Windows.Forms.Label achievStatus4;
        private System.Windows.Forms.Label achievStatus5;
        private System.Windows.Forms.Label achievStatus6;
        private System.Windows.Forms.Label label6;
    }
}