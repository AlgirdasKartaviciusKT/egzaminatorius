﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class register_page : Form
    {
        Controlers.userController userControl;
        home_page home_Page;
        login_page log_Page;

        public register_page()
        {
            InitializeComponent();
            passwordBox.PasswordChar = '\0';
            userControl = new Controlers.userController();
            regButton.Select();
        }

        public void open_register()
        {

        }

        public void submit_register()
        {
            errorBox.Hide();
            usernameBox.BackColor = Color.White;
            passwordBox.BackColor = Color.White;
            confirmBox.BackColor = Color.White;
            nameBox.BackColor = Color.White;
            surnameBox.BackColor = Color.White;
            emailBox.BackColor = Color.White;
            birthdayBox.CalendarTitleBackColor = Color.White;

            string username = usernameBox.Text;
            string password = passwordBox.Text;
            string confirm = confirmBox.Text;
            string name = nameBox.Text;
            string surname = surnameBox.Text;
            string email = emailBox.Text;
            DateTime birthday = birthdayBox.Value;

            if (userControl.check_session_id())
            {
                this.Hide();
                home_Page = new home_page();
                home_Page.Show();
            }
            else
            {
                if (username != "Username" && password != "Password" && confirm != "Confirm Passw." && name != "Name" && surname != "Surname" && email != "Email")
                {
                    string retReason = userControl.validate_reg(username, password, confirm, name, surname, email, birthday);
                    if (retReason == "Successfully registered!")
                    {
                        this.Hide();
                        log_Page = new login_page();
                        log_Page.Show();
                    }
                    else
                    {
                        errorBox.Show();
                        errorBox.Text = retReason;
                        if (retReason == "Username is already taken")
                            usernameBox.BackColor = Color.Red;
                        if (retReason == "Passwords do not match")
                        {
                            passwordBox.BackColor = Color.Red;
                            confirmBox.BackColor = Color.Red;
                        }
                        if (retReason == "Email is not valid")
                            emailBox.BackColor = Color.Red;
                        if (retReason == "Invalid birth day")
                            birthdayBox.CalendarTitleBackColor = Color.Red;
                    }
                }
                else
                    missingFieldsError(username, password, confirm, name, surname, email);
            }
        }

        public void missingFieldsError(string username, string password, string confirm, string name, string surname, string email)
        {
            if (username == "Username")
                usernameBox.BackColor = Color.Red;

            if (password == "Password")
                passwordBox.BackColor = Color.Red;

            if (confirm == "Confirm Passw.")
                confirmBox.BackColor = Color.Red;

            if (name == "Name")
                nameBox.BackColor = Color.Red;

            if (surname == "Surname")
                surnameBox.BackColor = Color.Red;

            if (email == "Email")
                emailBox.BackColor = Color.Red;
        }

        private void usernameBox_Enter(object sender, EventArgs e)
        {
            if (usernameBox.Text == "Username")
            {
                usernameBox.Text = "";
            }
        }

        private void usernameBox_Leave(object sender, EventArgs e)
        {
            if (usernameBox.Text == "")
            {
                usernameBox.Text = "Username";
            }
        }

        private void passwordBox_Enter(object sender, EventArgs e)
        {
            if (passwordBox.Text == "Password")
            {
                passwordBox.PasswordChar = '*';
                passwordBox.Text = "";
            }
        }

        private void passwordBox_Leave(object sender, EventArgs e)
        {
            if (passwordBox.Text == "")
            {
                passwordBox.PasswordChar = '\0';
                passwordBox.Text = "Password";
            }
        }

        private void confirmBox_Enter(object sender, EventArgs e)
        {
            if (confirmBox.Text == "Confirm Passw.")
            {
                confirmBox.PasswordChar = '*';
                confirmBox.Text = "";
            }
        }

        private void confirmBox_Leave(object sender, EventArgs e)
        {
            if (confirmBox.Text == "")
            {
                confirmBox.PasswordChar = '\0';
                confirmBox.Text = "Confirm Passw.";
            }
        }

        private void nameBox_Enter(object sender, EventArgs e)
        {
            if (nameBox.Text == "Name")
            {
                nameBox.Text = "";
            }
        }

        private void nameBox_Leave(object sender, EventArgs e)
        {
            if (nameBox.Text == "")
            {
                nameBox.Text = "Name";
            }
        }

        private void surnameBox_Enter(object sender, EventArgs e)
        {
            if (surnameBox.Text == "Surname")
            {
                surnameBox.Text = "";
            }
        }

        private void surnameBox_Leave(object sender, EventArgs e)
        {
            if (surnameBox.Text == "")
            {
                surnameBox.Text = "Surname";
            }
        }

        private void emailBox_Enter(object sender, EventArgs e)
        {
            if (emailBox.Text == "Email")
            {
                emailBox.Text = "";
            }
        }

        private void emailBox_Leave(object sender, EventArgs e)
        {
            if (emailBox.Text == "")
            {
                emailBox.Text = "Email";
            }
        }

        private void regButton_Click(object sender, EventArgs e)
        {
            submit_register();
        }

        private void loginLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            log_Page = new login_page();
            this.Hide();
            if (userControl.check_session_id())
            {
                home_Page = new home_page();
                home_Page.Show();
            }
            else
                log_Page.Show();
        }
    }
}
