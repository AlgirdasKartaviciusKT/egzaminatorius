﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egzaminatorius.Views
{
    public partial class user_edit_page : Form
    {
        Controlers.userController userControl;
        home_page home_Page;

        public user_edit_page()
        {
            userControl = new Controlers.userController();
            Models.User userData = userControl.edit_user();
            InitializeComponent();
            nameBox.Text = userData.name;
            surnameBox.Text = userData.surname;
            emailBox.Text = userData.email;
            birthdayBox.Value = userData.birth_date;
        }

        public void validate_edit()
        {
            nameBox.BackColor = Color.White;
            surnameBox.BackColor = Color.White;
            emailBox.BackColor = Color.White;
            birthdayBox.BackColor = Color.White;

            if (nameBox.Text != "" && surnameBox.Text != "" && emailBox.Text != "" && emailBox.Text.Contains('@') && birthdayBox.Value != DateTime.Today)
            {
                userControl.confirm_edit(nameBox.Text, surnameBox.Text, emailBox.Text, birthdayBox.Value);
            }
            else
            {
                missingFieldsError(nameBox.Text, surnameBox.Text, emailBox.Text, birthdayBox.Value);
            }
        }

        public void missingFieldsError(string name, string surname, string email, DateTime birthday)
        {
            if (name == "Name" || name == "")
                nameBox.BackColor = Color.Red;

            if (surname == "Surname" || surname == "")
                surnameBox.BackColor = Color.Red;

            if (email == "Email" || email == "" || !email.Contains('@'))
                emailBox.BackColor = Color.Red;

            if (birthday == DateTime.Today)
                birthdayBox.BackColor = Color.Red;
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            home_Page = new home_page();
            home_Page.Show();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            validate_edit();
        }

        private void user_edit_page_Load(object sender, EventArgs e)
        {

        }
    }
}
