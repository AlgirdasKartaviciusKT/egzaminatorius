namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        testId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        complexity = c.String(),
                        time = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.testId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tests");
        }
    }
}
