namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class topics_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.topics",
                c => new
                    {
                        name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.name);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.topics");
        }
    }
}
