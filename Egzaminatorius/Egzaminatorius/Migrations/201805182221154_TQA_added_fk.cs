namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TQA_added_fk : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PossibleTestAnswers", "fk_questionId", c => c.Int(nullable: false));
            AddColumn("dbo.TestQuestions", "fk_testId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestQuestions", "fk_testId");
            DropColumn("dbo.PossibleTestAnswers", "fk_questionId");
        }
    }
}
