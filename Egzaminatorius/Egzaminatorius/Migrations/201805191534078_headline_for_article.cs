namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class headline_for_article : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "headline", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "headline");
        }
    }
}
