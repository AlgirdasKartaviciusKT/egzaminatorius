namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        articleId = c.Int(nullable: false, identity: true),
                        content = c.String(),
                        likeCount = c.Int(nullable: false),
                        dislikeCount = c.Int(nullable: false),
                        articleState = c.String(),
                    })
                .PrimaryKey(t => t.articleId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Articles");
        }
    }
}
