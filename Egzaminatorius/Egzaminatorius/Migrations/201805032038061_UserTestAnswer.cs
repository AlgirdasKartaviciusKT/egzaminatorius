namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTestAnswer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserTestAnswers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        answer = c.String(),
                        points = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.id);

        }
        
        public override void Down()
        {
            DropTable("dbo.UserTestAnswers");
        }
    }
}
