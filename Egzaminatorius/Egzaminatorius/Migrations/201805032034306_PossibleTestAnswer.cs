namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PossibleTestAnswer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PossibleTestAnswers",
                c => new
                    {
                        answerId = c.Int(nullable: false, identity: true),
                        isCorrect = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.answerId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PossibleTestAnswers");
        }
    }
}
