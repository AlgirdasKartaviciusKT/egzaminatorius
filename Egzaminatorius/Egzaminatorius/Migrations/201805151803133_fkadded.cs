namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fkadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "fkstraipsnioId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comments", "fkstraipsnioId");
        }
    }
}
