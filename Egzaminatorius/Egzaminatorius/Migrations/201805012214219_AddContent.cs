namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContent : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "content", c => c.String(nullable: false, maxLength: 500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "content", c => c.String());
        }
    }
}
