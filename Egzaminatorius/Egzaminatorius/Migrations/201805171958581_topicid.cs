namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class topicid : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.topics");
            AddColumn("dbo.topics", "topicID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.topics", "name", c => c.String());
            AddPrimaryKey("dbo.topics", "topicID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.topics");
            AlterColumn("dbo.topics", "name", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.topics", "topicID");
            AddPrimaryKey("dbo.topics", "name");
        }
    }
}
