namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class article_fk_key : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "fkTopic", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "fkTopic");
        }
    }
}
