namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PossibleTestAnswer_added_answer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PossibleTestAnswers", "answer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PossibleTestAnswers", "answer");
        }
    }
}
