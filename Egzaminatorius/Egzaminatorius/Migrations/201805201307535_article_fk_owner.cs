namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class article_fk_owner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "fkOwnerID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "fkOwnerID");
        }
    }
}
