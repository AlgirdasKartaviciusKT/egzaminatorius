namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class k : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.user_achievement", "user_id", c => c.Int(nullable: false));
            AddColumn("dbo.user_achievement", "achiev_id", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.user_achievement", "achiev_id");
            DropColumn("dbo.user_achievement", "user_id");
        }
    }
}
