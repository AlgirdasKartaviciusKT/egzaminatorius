namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _5000 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "content", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "content", c => c.String(nullable: false, maxLength: 500));
        }
    }
}
