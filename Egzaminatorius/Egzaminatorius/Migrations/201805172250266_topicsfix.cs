namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class topicsfix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserTests", "fk_userId", c => c.Int(nullable: false));
            AddColumn("dbo.UserTests", "fk_testId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTests", "fk_testId");
            DropColumn("dbo.UserTests", "fk_userId");
        }
    }
}
