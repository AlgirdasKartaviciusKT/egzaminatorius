namespace Egzaminatorius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestQuestion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestQuestions",
                c => new
                    {
                        questionId = c.Int(nullable: false, identity: true),
                        question = c.String(),
                        type = c.String(),
                        value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.questionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TestQuestions");
        }
    }
}
