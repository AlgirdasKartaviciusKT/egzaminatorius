// <auto-generated />
namespace Egzaminatorius.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class UserTest_added_fk : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UserTest_added_fk));
        
        string IMigrationMetadata.Id
        {
            get { return "201805171831540_UserTest_added_fk"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
