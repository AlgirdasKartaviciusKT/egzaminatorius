﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class Article
    {
        public int articleId { get; set; }
        public String content { get; set; }
        public int likeCount { get; set; }
        public int dislikeCount { get; set; }
        public string articleState { get; set; }
        public string headline { get; set; }
        public string fkTopic { get; set; }
        public int fkOwnerID { get; set; }

        public static List<Article> SelectPending()
        {
            List<Article> articles = new List<Article>();

            using (SqlCommand select = new SqlCommand("SELECT * from Articles where articleState = 'Pending' ORDER BY fkTopic", MSSQL.Connection()))
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    articles.Add(new Article
                    {
                        articleId = reader.GetInt32(0),
                        content = reader.GetString(1),
                        likeCount = reader.GetInt32(2),
                        dislikeCount = reader.GetInt32(3),
                        articleState = reader.GetString(4),
                        headline = reader.GetString(5),
                        fkTopic = reader.GetString(6),
                        fkOwnerID = reader.GetInt32(7)
                    });
                }
            }
            return articles;
        }

        public static List<Article> Select(string category)
        {
            List<Article> articles = new List<Article>();

            using (SqlCommand select = new SqlCommand("SELECT * from Articles where fkTopic like '" + category + "' and articleState = 'Approved'", MSSQL.Connection()))
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    articles.Add(new Article
                    {
                        articleId = reader.GetInt32(0),
                        content = reader.GetString(1),
                        likeCount = reader.GetInt32(2),
                        dislikeCount = reader.GetInt32(3),
                        articleState = reader.GetString(4),
                        headline = reader.GetString(5),
                        fkTopic = reader.GetString(6),
                        fkOwnerID = reader.GetInt32(7)
                    });
                }
            }

            return articles;
        }

        public static List<Article> SelectDrafts(string category)
        {
            List<Article> articles = new List<Article>();

            using (SqlCommand select = new SqlCommand("SELECT * from Articles where fkTopic like '" + category + "' and articleState = 'Draft' and fkOwnerID = '" + sessionInfo.UserID + "'", MSSQL.Connection()))
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    articles.Add(new Article
                    {
                        articleId = reader.GetInt32(0),
                        content = reader.GetString(1),
                        likeCount = reader.GetInt32(2),
                        dislikeCount = reader.GetInt32(3),
                        articleState = reader.GetString(4),
                        headline = reader.GetString(5),
                        fkTopic = reader.GetString(6),
                        fkOwnerID = reader.GetInt32(7)
                    });
                }
            }

            return articles;
        }

        public static List<Article> SelectAllApproved()
        {
            List<Article> articles = new List<Article>();

            using (SqlCommand select = new SqlCommand("SELECT * from Articles where articleState = 'Approved' ORDER BY fkTopic", MSSQL.Connection()))
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    articles.Add(new Article
                    {
                        articleId = reader.GetInt32(0),
                        content = reader.GetString(1),
                        likeCount = reader.GetInt32(2),
                        dislikeCount = reader.GetInt32(3),
                        articleState = reader.GetString(4),
                        headline = reader.GetString(5),
                        fkTopic = reader.GetString(6),
                        fkOwnerID = reader.GetInt32(7)
                    });
                }
            }

            return articles;
        }

        public Article SelectArticle(string headline)
        {
            Article a = new Article(); 
            using (SqlCommand sqlCommand = new SqlCommand("SELECT * from Articles where headline = @headline", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@headline", headline);
                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        a = (new Article
                        {
                            articleId = reader.GetInt32(0),
                            content = reader.GetString(1),
                            likeCount = reader.GetInt32(2),
                            dislikeCount = reader.GetInt32(3),
                            articleState = reader.GetString(4),
                            headline = reader.GetString(5),
                            fkTopic = reader.GetString(6),
                            fkOwnerID = reader.GetInt32(7)
                        });
                    }
                }
            }
            return a;
        }
        public Article SelectArticleById(int headline)
        {
            Article a = new Article();
            using (SqlCommand sqlCommand = new SqlCommand("SELECT * from Articles where articleId = @headline", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@headline", headline);
                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        a = (new Article
                        {
                            articleId = reader.GetInt32(0),
                            content = reader.GetString(1),
                            likeCount = reader.GetInt32(2),
                            dislikeCount = reader.GetInt32(3),
                            articleState = reader.GetString(4),
                            headline = reader.GetString(5),
                            fkTopic = reader.GetString(6),
                            fkOwnerID = reader.GetInt32(7)
                        });
                    }
                }
            }
            return a;
        }
        public void InsertArticle(Article c)
        {

            DbIniatilizer.context.Articles.Add(c);


        }
        public void IncreamentLike(int id)
        {
            Article edit = new Article();
            foreach (var item in DbIniatilizer.context.Articles)
            {
                if (item.articleId == id)
                {
                    item.likeCount++;

                }
            }
            DbIniatilizer.context.SaveChanges();


        }
        public void IncreamentDislike(int id)
        {
            Article edit = new Article();
            foreach (var item in DbIniatilizer.context.Articles)
            {
                if (item.articleId == id)
                {
                    item.dislikeCount++;

                }
            }
            DbIniatilizer.context.SaveChanges();


        }

        public static bool SavePublication(string headline, string content, string category, int id)
        {
            string state = "Pending";
            return insert(headline, content, category, state, id);
        }

        public static bool SaveDraft(string headline, string content, string category, int id)
        {
            string state = "Draft";
            return insert(headline, content, category, state, id);
        }

        private static bool insert(string headline, string content, string category, string state, int id)
        {
            using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO Articles (content, likeCount, dislikeCount, articleState, headline, fkTopic, fkOwnerID) " +
                "VALUES (@content, 0, 0, @state, @headline, @fkTopic, @fkOwnerID)", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@content", content);
                sqlCommand.Parameters.AddWithValue("@state", state);
                sqlCommand.Parameters.AddWithValue("@headline", headline);
                sqlCommand.Parameters.AddWithValue("@fkTopic", category);
                sqlCommand.Parameters.AddWithValue("@fkOwnerID", id);
                sqlCommand.ExecuteNonQuery();
                return true;
            }
        }

        public static bool articleExists(string headline)
        {
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from Articles where headline like @headline", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@headline", headline);
                int count = (int)sqlCommand.ExecuteScalar();
                if (count > 0) return true;
                else return false;
            }

        }

        public static int IDfromHeadline(string headline)
        {
            using (SqlCommand sqlCommand = new SqlCommand("SELECT articleId from Articles where headline = @headline", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@headline", headline);
                return (int)sqlCommand.ExecuteScalar();
            }

        }

        public static List<Article> update(Article ar)
        {
            using (SqlCommand sqlCommand = new SqlCommand("UPDATE Articles SET headline = @headline, content = @content, likeCount = @likeCount, dislikeCount = @dislikeCount, articleState = @state, fkTopic = @topic " +
                "WHERE articleID = @ID", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@ID", ar.articleId);
                sqlCommand.Parameters.AddWithValue("@content", ar.content);
                sqlCommand.Parameters.AddWithValue("@likeCount", ar.likeCount);
                sqlCommand.Parameters.AddWithValue("@dislikeCount", ar.dislikeCount);
                sqlCommand.Parameters.AddWithValue("@state", ar.articleState);
                sqlCommand.Parameters.AddWithValue("@topic", ar.fkTopic);
                sqlCommand.Parameters.AddWithValue("@headline", ar.headline);
                sqlCommand.ExecuteNonQuery();
            }
            return SelectPending();
        }

        public static bool remove(string headline)
        {
            using (SqlCommand sqlCommand = new SqlCommand("UPDATE Articles SET articleState = 'Rejected' where headline = @headline", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@headline", headline);
                sqlCommand.ExecuteNonQuery();
                return true;
            }
        }
    }
}
