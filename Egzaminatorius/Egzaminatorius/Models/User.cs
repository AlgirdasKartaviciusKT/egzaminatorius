﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Egzaminatorius.Models
{
   public class User
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string role { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public DateTime birth_date { get; set; }
        public string status { get; set; }



        public int check_existing(string username, string password)
        {
            int userId = -1;
            using (SqlCommand select = new SqlCommand("SELECT * from Users WHERE username=(@usr) AND password=(@pass)", MSSQL.Connection()))
            {
                select.Parameters.AddWithValue("@usr", username);
                select.Parameters.AddWithValue("@pass", password);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    userId = reader.GetInt32(0);
                }
            }
            return userId;
        }

        public void block_user(int id)
        {
            using (SqlCommand update = new SqlCommand("UPDATE Users SET status =(@usrstatus) WHERE id=(@usrid)", MSSQL.Connection()))
            {
                update.Parameters.AddWithValue("@usrid", id);
                update.Parameters.AddWithValue("@usrstatus", "užblokuotas");
                update.ExecuteNonQuery();
            }
        }

        public List<User> search_for_matching_users(string line)
        {
            List<User> users = new List<User>();
            using (SqlCommand select = new SqlCommand("SELECT * from Users WHERE username Like (@line)", MSSQL.Connection()))
            {
                string newline = '%' + line + '%';
                select.Parameters.AddWithValue("@line", newline);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User
                    {
                        id = reader.GetInt32(0),
                        username = reader.GetString(1),
                        password = reader.GetString(2),
                        role = reader.GetString(3),
                        name = reader.GetString(4),
                        surname = reader.GetString(5),
                        email = reader.GetString(6),
                        birth_date = reader.GetDateTime(7),
                        status = reader.GetString(8)
                    });
                }
            }
            return users;
        }

        public int check_existing_username(string username)
        {
            int userId = -1;
            using (SqlCommand select = new SqlCommand("SELECT * from Users WHERE username=(@usr)", MSSQL.Connection()))
            {
                select.Parameters.AddWithValue("@usr", username);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    userId = reader.GetInt32(0);
                }
            }
            return userId;
        }

        public void update_status(int id)
        {
            string userStatus = "";
            using (SqlCommand select = new SqlCommand("SELECT status from Users WHERE id=(@usrid)", MSSQL.Connection()))
            {
                select.Parameters.AddWithValue("@usrid", id);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    userStatus = reader.GetString(0);
                }
            }

            if (userStatus == "Atsijungęs")
            {
                userStatus = "Prisijungęs";
            }
            else if (userStatus == "Prisijungęs")
            {
                userStatus = "Atsijungęs";
            }

            using (SqlCommand update = new SqlCommand("UPDATE Users SET status =(@usrstatus) WHERE id=(@usrid)", MSSQL.Connection()))
            {
                update.Parameters.AddWithValue("@usrid", id);
                update.Parameters.AddWithValue("@usrstatus", userStatus);
                update.ExecuteNonQuery();
            }
        }

        public bool check_status(int id)
        {
            string userStatus = "";
            using (SqlCommand select = new SqlCommand("SELECT status from Users WHERE id=(@usrid)", MSSQL.Connection()))
            {
                select.Parameters.AddWithValue("@usrid", id);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    userStatus = reader.GetString(0);
                }
            }
            if (userStatus == "Prisijungęs")
                return true;
            return false;
        }

        public void create_new(string username, string password, string confpassword, string name, string surname, string email, DateTime birthday)
        {
            using (SqlCommand update = new SqlCommand("INSERT INTO Users VALUES ((@username), (@pass), (@member), (@name), (@surname), (@email), (@birth), (@status))", MSSQL.Connection()))
            {
                update.Parameters.AddWithValue("@username", username);
                //update.Parameters.AddWithValue("@id", 2);
                update.Parameters.AddWithValue("@pass", password);
                update.Parameters.AddWithValue("@member", "member");
                update.Parameters.AddWithValue("@name", name);
                update.Parameters.AddWithValue("@surname", surname);
                update.Parameters.AddWithValue("@email", email);
                update.Parameters.AddWithValue("@birth", birthday);
                update.Parameters.AddWithValue("@status", "Atsijungęs");
                update.ExecuteNonQuery();
            }
        }

        public User find_user_data(int id)
        {
            User userData = new User();
            using (SqlCommand select = new SqlCommand("SELECT * from Users WHERE id=(@usrid)", MSSQL.Connection()))
            {
                select.Parameters.AddWithValue("@usrid", id);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    userData = new User
                    {
                        id = reader.GetInt32(0),
                        username = reader.GetString(1),
                        password = reader.GetString(2),
                        role = reader.GetString(3),
                        name = reader.GetString(4),
                        surname = reader.GetString(5),
                        email = reader.GetString(6),
                        birth_date = reader.GetDateTime(7),
                        status = reader.GetString(8)
                    };
                }
            }
            return userData;
        }

        public void update_info(int id, string name, string surname, string email, DateTime birthday)
        {
            using (SqlCommand update = new SqlCommand("UPDATE Users SET name=(@name), surname=(@surname), email=(@email), birth_date=(@birthday) WHERE id=(@usrid)", MSSQL.Connection()))
            {
                update.Parameters.AddWithValue("@usrid", id);
                update.Parameters.AddWithValue("@name", name);
                update.Parameters.AddWithValue("@surname", surname);
                update.Parameters.AddWithValue("@email", email);
                update.Parameters.AddWithValue("@birthday", birthday);
                update.ExecuteNonQuery();
            }
        }



    }


}
