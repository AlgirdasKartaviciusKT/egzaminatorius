﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Egzaminatorius.Models
{
    
    public class topic
    {
        [Key]
        public string name { get; set; }

        public static List<topic> SelectTopics()
        {
            List<topic> topics = new List<topic>();

            using (SqlCommand select = new SqlCommand("SELECT * from Topics", MSSQL.Connection()))
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    topics.Add(new topic
                    {
                        name = reader.GetString(0)
                    });
                }
            }
            return topics;
        }

        public static bool topicExists(string name)
        {
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from Topics where name like @name", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@name", name);
                int count = (int)sqlCommand.ExecuteScalar();
                if (count > 0) return true;
                else return false;
            }
                
        }

        public static bool insert(string name)
        {
            using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO Topics (name) VALUES (@name)", MSSQL.Connection()))
            {
                sqlCommand.Parameters.AddWithValue("@name", name);
                sqlCommand.ExecuteNonQuery();
                return true;
            }
        }
    }
}
