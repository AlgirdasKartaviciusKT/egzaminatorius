﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
   public class MSSQL
    {
        private static string conString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Egzaminatorius.Models.ModelDbContex;Integrated Security=True";
        public static SqlConnection Connection()
        {
            SqlConnection con = new SqlConnection(conString);
            con.Open();
            return con;

        }
    }
   
}
