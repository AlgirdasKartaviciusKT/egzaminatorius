﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class Test
    {
        public int testId { get; set; }
        public string name { get; set; }
        public string complexity { get; set; }
        public int time { get; set; } // min.

        public List<Test> selectTests()
        {
            List<Test> tests = new List<Test>();
            SqlCommand select = new SqlCommand("SELECT * FROM Tests", MSSQL.Connection());

            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    tests.Add(new Test
                    {
                        testId = reader.GetInt32(0),
                        name = reader.GetString(1),
                        complexity = reader.GetString(2),
                        time = reader.GetInt32(3)
                    });
                }
            }

            return tests;
        }

        public Test getTestInfo(int id)
        {
            SqlCommand select = new SqlCommand("SELECT * FROM Tests WHERE testId=@id", MSSQL.Connection());
            select.Parameters.Add("id", id);

            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    Test info = new Test
                    {
                        testId = reader.GetInt32(0),
                        name = reader.GetString(1),
                        complexity = reader.GetString(2),
                        time = reader.GetInt32(3)
                    };
                    return info;
                }
            }
            
            return null;
        }

        public void insertTestData(Test data)
        {
            DbIniatilizer.context.Tests.Add(data);
            DbIniatilizer.context.SaveChanges();
        }

        public void deleteTest(int id)
        {
            if (id > 0)
            {
                DbIniatilizer.context.Tests.Remove(DbIniatilizer.context.Tests.Find(id));
                DbIniatilizer.context.SaveChanges();
            }
        }

        public override string ToString()
        {
            return String.Format("{0}.\t {1}\t {2}\t {3} min.", testId, name, complexity, time);
        }
    }
}
