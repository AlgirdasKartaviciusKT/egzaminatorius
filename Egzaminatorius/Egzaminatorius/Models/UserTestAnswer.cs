﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class UserTestAnswer
    {
        public int id { get; set; }
        public string answer { get; set; }
        public double points { get; set; }
    }
}
