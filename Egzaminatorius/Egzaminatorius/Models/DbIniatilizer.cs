﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class DbIniatilizer
    {
        public static ModelDbContex context = new ModelDbContex();
        public static void Seed()
        {

            if (!context.Topics.Any())
            {
                context.Topics.Add(new Models.topic { name = "Informatika" });
                context.Topics.Add(new Models.topic { name = "Matematika" });
            }

            if (!context.Articles.Any())
                {
                    context.Articles.Add(new Models.Article { content = "straipsnis10", likeCount = 0, dislikeCount = 0, articleState = "neskaitytas", headline = "straipsnis1", fkTopic = "Informatika", fkOwnerID = 1 });
                    context.Articles.Add(new Models.Article { content = "straipsnis", likeCount = 0, dislikeCount = 0, articleState = "neskaitytas", headline = "straipsnis0", fkTopic = "Matematika", fkOwnerID = 2 });
                }

                if (!context.Comments.Any())
                {
                    context.Comments.Add(new Models.Comment {  content = "test", like = 0, dislike = 1, fkstraipsnioId = 1 });
                    context.Comments.Add(new Models.Comment { content = @"Didelę dalį pratybų dalyvių sudarys būtent JAV kariai – nuo 5 iki 7 tūkst.karių.Pavyzdžiui,
                        Lietuvos ir Lenkijos sieną dviem keliais kirs 2 - ojo kavalerijos pulko kolona sudaryta iš 900 – 1100 karinės technikos vienetų,
                        Rukloje išsilaipins tiesiai iš JAV atskraidinta parašiutininkų kuopa.

Ir tai turėtų būti tik dalis karinių pratybų,
                        kurios vienu metu vyks ne tik Lietuvos teritorijoje,
                        bet ir kaimyninėse šalyse.Vienu metu Lietuvoje pratybų dalyvių skaičius neturėtų viršyti 13 tūkst.karių.

Iš anksto įspėja gyventojus

„Vienu metu Lietuvos teritorijoje,
                        oro erdvėje ir teritoriniuose vandenyse bus daug veiksmo,
                        Lietuvos keliai bus apkrauti“, – pripažino kariuomenės Gynybos štabo karininkas,
                        pulkininkas Vilmas Šatas,
                        atsakingas už Lietuvoje vykstančias pratybas.

Tai,
                        kad Lietuvoje birželį vienu metu treniruosis daugiausiai karių nuo įstojimo į NATO ir bus dienų kai JAV karių skaičius bus didesnis dėl judėjimo per Lietuvą,
                        Estiją ir Latviją,
                        dar šių metų pradžioje žadėjo kariuomenės Gynybos štabo atstovas kapitonas Tomas Pakalniškis.", like = 0, dislike = 1, fkstraipsnioId = 2});


            }

            if (!context.Tests.Any())
            {
                context.Tests.Add(new Models.Test { name = "Informatika. 1 dalis", complexity = "lengvas", time = 20 });
                context.Tests.Add(new Models.Test { name = "Matematika. 1 dalis", complexity = "lengvas", time = 25 });
                context.Tests.Add(new Models.Test { name = "Informatika. 2 dalis", complexity = "vidutinis", time = 40 });
                context.Tests.Add(new Models.Test { name = "Biologija", complexity = "vidutinis", time = 35 });
                context.Tests.Add(new Models.Test { name = "Operacinės sistemos", complexity = "sudėtingas", time = 60 });
            }

            if (!context.UserTests.Any())
            {
                context.UserTests.Add(new Models.UserTest { status = "Neišlaikyta", points = 1, fk_userId = 1, fk_testId = 1 });
                context.UserTests.Add(new Models.UserTest { status = "Išlaikyta", points = 10, fk_userId = 2, fk_testId = 2 });
            }

            if (!context.User.Any())
            {
                context.User.Add(new Models.User {  username = "usr1", password = "pass1", role = "member", name = "Name1", surname = "Surname1", email = "a@a", birth_date = DateTime.Today, status = "atsijungęs" });
                context.User.Add(new Models.User { username = "admin", password = "admin", role = "admin", name = "Name1", surname = "Surname1", email = "a@a", birth_date = DateTime.Today, status = "atsijungęs" });
        }
            if (!context.user_Achievement.Any())
            {
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 1, user_id = 1 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 2, user_id = 1 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 3, user_id = 1 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 4, user_id = 1 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 5, user_id = 1 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 6, user_id = 1 });

                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 1, user_id = 2 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 2, user_id = 2 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 3, user_id = 2 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 4, user_id = 2 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 5, user_id = 2 });
                context.user_Achievement.Add(new Models.user_achievement { status = "locked", achiev_id = 6, user_id = 2 });
            }

            context.SaveChanges();

            
        }
    }
}
