﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Egzaminatorius.Models
{
   public class user_achievement
    {
        public int id { get; set; }
        public string status { get; set; }
        public int user_id { get; set; }
        public int achiev_id { get; set; }

        public List<user_achievement> get_user_achievements(int id)
        {
            List<user_achievement> achievs = new List<user_achievement>();
            using (SqlCommand select = new SqlCommand("SELECT * from user_achievement WHERE user_id=(@usrid)", MSSQL.Connection()))
            {
                select.Parameters.AddWithValue("@usrid", id);
                SqlDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    achievs.Add(new user_achievement
                    {
                        id = reader.GetInt32(0),
                        status = reader.GetString(1),
                        user_id = reader.GetInt32(2),
                        achiev_id = reader.GetInt32(3)
                    });
                }
            }
            return achievs;
        }

        public void unmark(int u_id)
        {
            using (SqlCommand update = new SqlCommand("UPDATE user_achievement SET status =(@achiev_status) WHERE user_id=(@u_id) AND status=(@oldstatus)", MSSQL.Connection()))
            {
                update.Parameters.AddWithValue("@u_id", u_id);
                update.Parameters.AddWithValue("@achiev_status", "unlocked");
                update.Parameters.AddWithValue("@oldstatus", "marked");
                update.ExecuteNonQuery();
            }
        }

        public void mark(int a_id, int u_id)
        {
            using (SqlCommand update = new SqlCommand("UPDATE user_achievement SET status =(@achiev_status) WHERE achiev_id=(@a_id) AND user_id=(@u_id)", MSSQL.Connection()))
            {
                update.Parameters.AddWithValue("@a_id", a_id);
                update.Parameters.AddWithValue("@u_id", u_id);
                update.Parameters.AddWithValue("@achiev_status", "marked");
                update.ExecuteNonQuery();
            }
        }

    }

}
