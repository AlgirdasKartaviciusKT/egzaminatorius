﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class UserTest
    {
        public int id { get; set; }
        public string status { get; set; }
        public double points { get; set; }
        public int fk_userId { get; set; }
        public int fk_testId { get; set; }

        // Pasirinkto testo laikiusiųjų sąrašas
        public List<User> getTestUsers(int id)
        {
            List<User> users = new List<User>();
            SqlCommand select = new SqlCommand("SELECT * FROM Users INNER JOIN UserTests ON Users.id=UserTests.fk_userId " +
                "LEFT JOIN Tests ON UserTests.fk_testId=Tests.testId " +
                "WHERE Tests.testId=@testId", MSSQL.Connection());
            select.Parameters.Add("testId", id);

            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    users.Add(new User
                    {
                        name = reader.GetString(4),
                        surname = reader.GetString(5)
                    });
                }
            }

            return users;
        }

        public List<UserTest> getTestResult(int testId, int userId)
        {
            List<UserTest> info = new List<UserTest>();
            SqlCommand select = new SqlCommand("SELECT status, points FROM UserTests INNER JOIN Tests ON UserTests.fk_testId=Tests.testId " +
                "WHERE UserTests.fk_testId=@testId AND UserTests.fk_userId=@userId", MSSQL.Connection());
            select.Parameters.Add("testId", testId);
            select.Parameters.Add("userId", userId);

            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    info.Add(new UserTest
                    {
                        status = reader.GetString(0),
                        points = reader.GetDouble(1)
                    });
                }
            }

            return info;
        }

        public void insertSolvingInfo(UserTest data)
        {
            DbIniatilizer.context.UserTests.Add(data);
            DbIniatilizer.context.UserTests.Add(data);
        }
    }
}
