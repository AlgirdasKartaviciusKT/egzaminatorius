﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace Egzaminatorius.Models
{
    public class Comment
    {   [Key]
        public int commentId { get; set; }
        [Required]
        [MaxLength(5000)]
        public string content { get; set; }
        public int like { get; set; }
        public int dislike { get; set; }
        public int fkstraipsnioId { get; set;}

        public List<Comment> SelectComments(int fkstraipsnioId)
        {
            
            var comments = DbIniatilizer.context.Comments
              .Where(b => b.fkstraipsnioId == fkstraipsnioId)
                        .ToList();
            return comments;
        }
        public string Select(int id)
        {

           
            return DbIniatilizer.context.Comments.Find(id).content;
            
           
        }
        public void InsertComment(Comment c)
        {
            
                DbIniatilizer.context.Comments.Add(c);
            DbIniatilizer.context.SaveChanges();

              
        }
        public void DeleteComment(int id)
        {
            Comment forDelete = new Comment();
            foreach (var item in DbIniatilizer.context.Comments)
            {
                if (item.commentId == id)
                    forDelete = item;
            }
            if (forDelete.commentId > 0)
            {
                DbIniatilizer.context.Comments.Remove(forDelete);
                DbIniatilizer.context.SaveChanges();
            }
        }
        public void IncreamentLike(int id)
        {
            Comment edit = new Comment();
            foreach (var item in DbIniatilizer.context.Comments)
            {
                if (item.commentId == id)
                {
                    item.like++;
                   
                }
            }
            DbIniatilizer.context.SaveChanges();


        }
        public void IncreamentDislike(int id)
        {
            Comment edit = new Comment();
            foreach (var item in DbIniatilizer.context.Comments)
            {
                if (item.commentId == id)
                {
                    item.dislike++;

                }
            }
            DbIniatilizer.context.SaveChanges();


        }

    }
}
