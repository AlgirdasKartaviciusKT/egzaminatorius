﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class TestQuestion
    {
        [Key]
        public int questionId { get; set; }
        public string question { get; set; }
        public string type { get; set; }
        public double value { get; set; }
        public int fk_testId { get; set; }

        public void insertTestQuestions(TestQuestion data)
        {
            DbIniatilizer.context.TestQuestions.Add(data);
            DbIniatilizer.context.SaveChanges();
        }
    }
}
