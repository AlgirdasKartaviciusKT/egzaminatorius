﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class ModelDbContex :DbContext
    {
        public DbSet<Models.Comment> Comments { get; set; }
        public DbSet<Models.topic> Topics { get; set; }
        public DbSet<Models.Article> Articles { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestQuestion> TestQuestions { get; set; }
        public DbSet<PossibleTestAnswer> PossibleTestAnswers { get; set; }
        public DbSet<UserTest> UserTests { get; set; }
        public DbSet<UserTestAnswer> UserTestAnswers { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<user_achievement> user_Achievement { get; set; }
    }
}
