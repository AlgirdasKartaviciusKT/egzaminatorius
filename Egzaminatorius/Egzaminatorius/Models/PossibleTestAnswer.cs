﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egzaminatorius.Models
{
    public class PossibleTestAnswer
    {
        [Key]
        public int answerId { get; set; }
        public string answer { get; set; }
        public int isCorrect { get; set; }
        public int fk_questionId { get; set; }

        public void insertTestAnswers(PossibleTestAnswer data)
        {
            DbIniatilizer.context.PossibleTestAnswers.Add(data);
            DbIniatilizer.context.SaveChanges();
        }
    }
}
